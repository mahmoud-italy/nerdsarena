$(function(){
$(".line").typed({strings:["<span style='color:#fff'> public </span> <span style='color:#e84c3d'>function </span> <span style='color:#fff'>getRetailers</span> <span style='color:#e84c3d'>( <span style='color:orange'> $city </span>)</span> <span style='color:#e84c3d'> {</span> <br/> <span style='color:orange'> $locations </span> <span style='color:#e63de8'>=</span> <span style='color:#fff'> DB::table</span> <span style='color:#e84c3d'>('retailers_listings')</span> <br> <span style='color:#e63de8'> -> </span> <span style='color:#fff'>orderBy </span> <span style='color:#e84c3d'> ('country' <span style='color:#fff'> , </span> 'asc')</span> <br> <span style='color:#e63de8'> -> </span> <span style='color:#fff'>Where</span> <span style='color:#e84c3d'> ('city' <span style='color:#fff'> , </span> <span style='color:orange'>$city</span>) </span>  <br> <span style='color:#e63de8'> -> </span> <span style='color:#fff'>get</span> <span style='color:#e84c3d'>()</span> <span style='color:#fff'>;</span> <br> <span style='color:orange'>$this</span> <span style='color:#e63de8'> -> </span> <span style='color:#fff'>layout</span>  <span style='color:#e63de8'> -></span> <span style='color:#fff'>title</span>  <span style='color:#e63de8'> = </span> <span style='color:#e84c3d'>'Stores - '</span> <span style='color:#fff'>.</span> <span style='color:orange'>$city</span> <span style='color:#fff'>.</span> <span style='color:#e84c3d'>''</span> <span style='color:#fff'>;</span> <br> <span style='color:orange'>$this</span> <span style='color:#e63de8'> -> </span> <span style='color:#fff'>layout</span>  <span style='color:#e63de8'> -></span> <span style='color:#fff'>title_city</span>  <span style='color:#e63de8'> = </span> <span style='color:#e84c3d'>'Stores - '</span> <span style='color:#fff'>.</span> <span style='color:orange'>$city</span> <span style='color:#fff'>;</span> <br> <span style='color:orange'>$this</span> <span style='color:#e63de8'> -> </span> <span style='color:#fff'>layout</span>  <span style='color:#e63de8'> -></span> <span style='color:#fff'>description </span>  <span style='color:#e63de8'> = </span> <span style='color:#e84c3d'>''</span> <span style='color:#fff'>.</span> <span style='color:orange'>$city</span>  <span style='color:#fff'>.</span> <span style='color:#e84c3d'>'  stores in '</span> <span style='color:orange'>$city</span>  <span style='color:#fff'>.</span> <span style='color:#e84c3d'>''</span> <span style='color:#fff'>;</span> <br> <span style='color:orange'>$content</span> <span style='color:#e63de8'> -> </span> <span style='color:#fff'>View::make</span> <span style='color:#e84c3d'>('retailers.stores')</span> <br>  <span style='color:#e63de8'> -></span> <span style='color:#e84c3d'>('store_listings'</span> <span style='color:#fff'>,</span> <span style='color:orange'> $locations</span> <span style='color:#e84c3d'>)</span> <br> <span style='color:#e63de8'> -></span> <span style='color:#e84c3d'>('retailers_listings'</span> <span style='color:#fff'>,</span> <span style='color:orange'>  $this </span> <span style='color:#e63de8'> -> </span> <span style='color:#fff'>pagesElements</span>  <span style='color:#e63de8'> -></span> <span style='color:#fff'>RetailersNavigation</span> <span style='color:#e84c3d'>()</span><span style='color:#e84c3d'>)</span> <br> <span style='color:#e63de8'> -></span> <span style='color:#e84c3d'>('title'</span> <span style='color:#fff'>,</span> <span style='color:orange'> $this </span> <span style='color:#e63de8'> -></span> <span style='color:#fff'>layout</span> <span style='color:#e63de8'> -></span> <span style='color:#fff'>title</span> <span style='color:#e84c3d'>)</span> <br> <span style='color:#e63de8'> -></span> <span style='color:#e84c3d'>('title_city'</span> <span style='color:#fff'>,</span> <span style='color:orange'> $this </span> <span style='color:#e63de8'> -></span> <span style='color:#fff'>layout</span> <span style='color:#e63de8'> -></span> <span style='color:#fff'>title_city</span> <span style='color:#e84c3d'>)</span> <br> <span style='color:#e63de8'> -></span> <span style='color:#e84c3d'>('description'</span> <span style='color:#fff'>,</span> <span style='color:orange'> $this </span> <span style='color:#e63de8'> -></span> <span style='color:#fff'>layout</span> <span style='color:#e63de8'> -></span> <span style='color:#fff'>description</span> <span style='color:#e84c3d'>)</span> <span style='color:#fff'>;</span>"],typeSpeed:10
		});
});
var tl = new TimelineMax();
var bgd = $('#background rect');
var table = $('#table_legs, #table');
var lampLeg = $('#lamp > .lamp-leg');
var lampbt = $('#lamp-bottom');
var lampLight = $('#lamp > .light');
var lampLine = $('#lamp-line');
var lampLineB = $('#lamp-line-b');
var lampLineT = $('#lamp-line-t');
var lampCircle = $('#lamp-circle');
var lampHead = $('#lamp-head');
var lampHeader = $('#lamp-header');
var lampBody = $('#lamp-body');
var computer = $('#computer > *');
var keyboard = $('#keyboard > *');
var asset = $('#computer_mouse > * , #coffee_mug > *');
tl.from(bgd, 0.2, {opacity:0, scale:0, transformOrigin: 'center center'})
	.staggerFrom(table, 0.2, {y:"-=200", opacity: 0, ease: Elastic.easeOut}, 0.1)
	.from(lampLeg, 0.2, {opacity:0, x: "-200", ease: Elastic.easeOut})
	.from(lampbt, 0.2, {opacity:0, scale:0, transformOrigin: 'center center'})
	.from(lampLineB, 0.3,{opacity:0, transformOrigin: '100% 100%', rotation: '-180deg'})
	.from(lampCircle,0.1,{opacity:0, x: '-=100', y: '-=100'})
	.from(lampLineT, 0.3,{opacity:0, transformOrigin: '0% 100%', rotation: '-180deg'})
	.from(lampHead, 0.2, {opacity:0, scale:0, ease: Elastic.easeOut})
	.from(lampHeader, 0.5, {transformOrigin: '60% 60%', rotation: '60deg'})
	.from(lampBody, 0.5, {transformOrigin: '70% 70%', rotation: '-25deg'})
	.staggerFrom(computer, 1, {opacity: 0, scale: 0, transformOrigin: 'center center', ease: Back .easeOut}, 0.2)
	.staggerFrom(keyboard, 0.5, {opacity: 0, y: '-=100', ease: Linear.easeInOut }, 0.05)
	.staggerFrom(asset, 0.5, {opacity: 0}, 0.05)
	.to(lampLight, 0.2, {opacity:0.8, ease: Elastic.easeOut, delay:0.5}, "a")
	.to(lampLight, 0.1, {opacity:0}, "b")
	.to(lampLight, 0.1, {opacity:0.2}, "c")
	.fromTo(lampLine, 0.2, {opacity: 0},{opacity: 0.2, delay:0.5}, "a-=0.05")
	.to(lampLine, 0.1, {opacity: 1}, "b-=0.05")
	.to(lampLine, 0.1, {opacity: 0.5}, "c-=0.05")
	.to(bgd, 0.2, {opacity: 0.1, delay:0.5}, "a-=0.05")
	.to(bgd, 0.1, {opacity: 1}, "b-=0.05")
	.to(bgd, 0.1, {opacity: 0.5}, "c-=0.05")
	.to(bgd, 0.2, {opacity: 1, fill: ''})

	$(".-cu-panel").css("height" ,$(".-cu-item").css("height"))
$(".-open").click(function () {
	$(this).parent().next().toggleClass("top0")
})
$(".-close").click(function () {
	$(this).parent().toggleClass("top0")
})
$(document).ready(function(){(function($,window,undefined){'use strict';var Modernizr=window.Modernizr;jQuery.fn.reverse=[].reverse;$.SwatchBook=function(options,element){this.$el=$(element);this._init(options);};$.SwatchBook.defaults={center:6,angleInc:8,speed:700,easing:'ease',proximity:45,neighbor:4,onLoadAnim:true,initclosed:false,closeIdx:-1,openAt:-1};$.SwatchBook.prototype={_init:function(options){this.options=$.extend(true,{},$.SwatchBook.defaults,options);this.$items=this.$el.children('div');this.itemsCount=this.$items.length;this.current=-1;this.support=Modernizr.csstransitions;this.cache=[];if(this.options.onLoadAnim){this._setTransition();}if(!this.options.initclosed){this._center(this.options.center,this.options.onLoadAnim);}else{this.isClosed=true;if(!this.options.onLoadAnim){this._setTransition();}}if(this.options.openAt>=0&&this.options.openAt<this.itemsCount){this._openItem(this.$items.eq(this.options.openAt));}this._initEvents();},_setTransition:function(){if(this.support){this.$items.css({'transition':'all '+this.options.speed+'ms '+this.options.easing});}},_openclose:function(){this.isClosed?this._center(this.options.center,true):this.$items.css({'transform':'rotate(0deg)'});this.isClosed=!this.isClosed;},_center:function(idx,anim){var self=this;this.$items.each(function(i){var transformStr='rotate('+(self.options.angleInc*(i-idx))+'deg)';$(this).css({'transform':transformStr});});},_openItem:function($item){var itmIdx=$item.index();if(itmIdx!==this.current){if(this.options.closeIdx!==-1&&itmIdx===this.options.closeIdx){this._openclose();this._setCurrent();}else{this._setCurrent($item);$item.css({'transform':'rotate(0deg)'});this._rotateSiblings($item);}}},_initEvents:function(){var self=this;this.$items.on('click.swatchbook',function(event){self._openItem($(this));});},_rotateSiblings:function($item){var self=this,idx=$item.index(),$cached=this.cache[idx],$siblings;if($cached){$siblings=$cached;}else{$siblings=$item.siblings();this.cache[idx]=$siblings;}$siblings.each(function(i){var rotateVal=i<idx?self.options.angleInc*(i-idx):i-idx===1?self.options.proximity:self.options.proximity+(i-idx-1)*self.options.neighbor;var transformStr='rotate('+rotateVal+'deg)';$(this).css({'transform':transformStr});});},_setCurrent:function($el){this.current=$el?$el.index():-1;this.$items.removeClass('ff-active');if($el){$el.addClass('ff-active');}}};var logError=function(message){if(window.console){window.console.error(message);}};$.fn.swatchbook=function(options){var instance=$.data(this,'swatchbook');if(typeof options==='string'){var args=Array.prototype.slice.call(arguments,1);this.each(function(){if(!instance){logError("cannot call methods on swatchbook prior to initialization; "+"attempted to call method '"+options+"'");return;}if(!$.isFunction(instance[options])||options.charAt(0)==="_"){logError("no such method '"+options+"' for swatchbook instance");return;}instance[options].apply(instance,args);});}else{this.each(function(){if(instance){instance._init();}else{instance=$.data(this,'swatchbook',new $.SwatchBook(options,this));}});}return instance;};})(jQuery,window);$(function(){;$(".color-pic").on("change",function(){$(".color-pick li.active").css("background",$(this).val()).removeClass("active");$(".color-pick").append("<li class='active'><i class='fa fa-close remove-color'></i></li>");$(".remove-color").on("click",function(){$(this).parent("").remove()})});});$(function(){$(".add-color").on("click",function(){var inpColor=$(".hex-codes").val();if(inpColor==""){alert("الرجاء اضافة كود اللون")
return false}$(".color-pick li.active").css("background",inpColor).removeClass("active");$(".hex-codes").val("")
$(".color-pick").append("<li class='active'><i class='fa fa-close remove-color'></i></li>");})});$(function(){$(".add-website").on("click",function(){var inpWebsite=$(".website-url").val();if(inpWebsite==""){alert("لا يمكنك اضافة حقل فارغ")
return false}$(".websites li.active").append(inpWebsite).removeClass("active");$(".website-url").val("")
$(".websites").append("<li class='active'><i class='fa fa-close remove-website'></i></li>");$(".remove-website").on("click",function(){$(this).parent("").remove()})})});});$(window).load(function(){
    $(".spinner2").fadeOut(400,function(){$(this).remove()});
		$(".-tap-button").click(function () {
			$(".-menu").toggleClass("right-0")
		});

		$(document).scroll(function() {
		var scroll = $(this).scrollTop();
		if (scroll >= 1314) {
$(".stepssf").addClass("step-animate")
		}
		});

$.fn.isOnScreen = function(){
    var win = $(window);
    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();
    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();
    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
};
    $(window).scroll(function(){
        if ($('.stepss').isOnScreen()) {
$(".stepss").addClass('step-animate');
        } else {
					$(".stepss").removeClass('step-animate');
        }
    });
	})
    $(window).scroll(function(){
        if ($('._serv-section').isOnScreen()) {
$("._serv-section , -serv-left-content").addClass('bag-animate');
        }
    });
