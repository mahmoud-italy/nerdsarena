<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('meta_title');
            $table->string('meta_title_en');
            $table->string('meta_desc');
            $table->string('meta_desc_en');
            $table->string('meta_key');
            $table->string('meta_key_en');
            $table->string('meta_author');
            $table->string('meta_author_en');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seo');
    }
}
