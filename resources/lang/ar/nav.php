<?php 
return [
  'home' => 'الرئيسية',
  'services' => 'الخدمات',
  'works' => 'أعمالنا',
  'ask_work' => 'أطلب خدمة',
  'about' => 'من نحن',
  'contact' => 'أتصل بنا',
 ];