<?php 
return [

 /* Footer */
  'about' => 'عن الشركة',
  'pages' => 'روابط',
  'contact' => 'إتصل بنا',
  'copyright' => 'جميع الحقوق محفوظة لشركة نيردز أرينا',

 /* Clients */ 
   'clients' => 'عملاؤنا',

 /* Videos */
   'contact_us' => 'إتصل بنا',

 /* Sections */
    'services' => 'أطلب خدمة الآن',
    'programming' => 'البرمجة',
    'mobile' => 'الهاتف المحمول',
    'web' => 'تطبيقات الويب',

 /* Order Services */
    'name' => 'الأسم',
    'email' => 'البريد الإلكتروني',
    'phone' => 'الهاتف',
    'choice_serv' => 'أختر خدمة',
    'choice_colors' => 'اختر الوان موقعك ( اختياري )',
    'upload_details' => 'اذا كان لديك تفاصيل جاهزة او ملف وورد او pdf قم برفعه ( اختياري )',
    'upload_file' => 'أرف الملف',
    'your_colors' => 'الألوان التي اخترتها ',
    'your_msg' => 'الرسالة',
    'send' => 'إرسال', 
    'success_msg' => 'تم إرسال رسالتك بنجاح',
    'error_msg' => 'لم يتم إرسال الرسالة',
    'serv_msg_success' => 'تم إرسال طلبك بنجاح',
    'serv_msg_error' => 'لم تم إرسال طلبك',

   /* Contact */
    'msg_title' => 'عنوان الرسالة',
 ];