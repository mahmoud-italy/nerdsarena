<?php 
return [

 /* Footer */
  'about' => 'About',
  'pages' => 'Pages',
  'contact' => 'Contact',
  'copyright' => 'copyright nerdsarena.com 2015',

 /* Clients */ 
   'clients' => 'Clients',

 /* Videos */
   'contact_us' => 'Contact Us',

 /* Sections */
    'services' => 'Order Service',
    'programming' => 'Programming',
    'mobile' => 'Mobile App',
    'web' => 'Web App',

 /* Order Services */
    'name' => 'Name',
    'email' => 'Email',
    'phone' => 'Phone',
    'choice_serv' => 'Choice Service',
    'choice_colors' => 'Choice Colors ( Optional )',
    'upload_details' => 'if you had pdf, docx for details ( Optional )',
    'upload_file' => 'Upload File',
    'your_colors' => 'Colors chosen',
    'your_msg' => 'Message',
    'send' => 'Send', 
    'success_msg' => 'Message Sent Successfully',
    'error_msg' => 'Message Not Sent',
    'serv_msg_success' => 'Order Sent Successfully',
    'serv_msg_error' => 'Order Not Sent',

/* Contact */
    'msg_title' => 'Title',

 ];