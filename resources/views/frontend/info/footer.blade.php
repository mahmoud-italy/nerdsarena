  <div class="clear"></div>
  <footer class="_footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="-right-side footer-about">
            <span>{{ lang('customize.about') }}</span>
       @foreach(App\Setting::where('key','about')->get() as $about)
            <p>@if(!$about) {{ '' }} @else {!! $about->value !!} @endif</p>
       @endforeach
          </div>
        </div>
        <div class="col-sm-3">
          <div class="-right-side">
            <span>{{ lang('customize.pages') }}</span>
            <ul class="-footer-menu">
        @foreach(App\Page::orderby('sort','ASC')->get() as $page)    
          <li><a href="{{ 'page/'.$page->link }}" target="_blank">{!! model_translate($page,'name') !!}</a></li>
        @endforeach
            </ul>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="-right-side">
            <span>{{ lang('customize.contact') }}</span>
            <ul class="-footer-menu contact-info">

        @foreach(App\Setting::where('key','email')->get() as $email)
          <li><span>@if(!$email) {{ '' }} @else {!! $email->value !!} @endif</span></li>
        @endforeach
        @foreach(App\Setting::where('key','phone')->get() as $phone)
          <li><span>@if(!$phone) {{ '' }} @else {!! $phone->value !!} @endif</span></li>
        @endforeach
            </ul>
          </div>
        </div>
        <div class="col-sm-4">
          <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script><div style='overflow:hidden;height:250px;width:100%;'><div id='gmap_canvas' style='height:250px;width:100%;'></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div> <a href='https://embed-map.org/'>https://embed-map.org</a> <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=0ed38c632bf17c30ff2d6d65833672ac487a626c'></script><script type='text/javascript'>function init_map(){var myOptions = {zoom:12,center:new google.maps.LatLng(30.026200369650883,31.224589347839366),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(30.026200369650883,31.224589347839366)});infowindow = new google.maps.InfoWindow({content:'<strong></strong><br><br> <br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
        </div>

      </div>
    </div>
  </footer>