@foreach(App\Video::get() as $video)
<section class="_video-section">
  <div class="-cover">
   <div class="container">
     <div class="row">
       <div class="col-sm-7 ">
                    <!-- 16:9 aspect ratio -->
          <div class="border-frame">
            <div class="embed-responsive embed-responsive-16by9 ">
            <iframe width="360" height="250" src="https://www.youtube.com/embed/{!! $video->link !!}" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>

        </div>
        
        <div class="col-sm-5">
           <div class="-video-disc">
              <h4>{!! model_translate($video,'title') !!}</h4>
              <p>{!! model_translate($video,'content') !!}</p>
      
              <div class="clear"></div>
              <div class="clear"></div>

             <a href="{{ url('contact') }}" class="btn contact-us col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2  waves-effect  fix-button ">{{ lang('customize.contact_us') }}</a>
             
            </div>
        </div>

    </div>
  </div>
 </div>
</section>
@endforeach