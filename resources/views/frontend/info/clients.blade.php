<div class="clear"></div>

<section class="_logo-ticker">
  <div class="container">
    <div class="row">
      <article class="-head-logo">
        <h4>{{ lang('customize.clients') }}</h4>
      </article>

    <ul class="bxslider">

    @foreach(App\Client::orderby('sort','ASC')->get() as $clients)
      <li><a href="{{ $clients->link }}" target="_blank"><img src="{{ url($clients->logo) }}" style="border:1px solid #ddd;" alt="{!! $clients->name !!}" /></a></li>
    @endforeach

    </ul>

  </div>
 </div>
</section>