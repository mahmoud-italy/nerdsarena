<section class="-custumer">
    <div class="container">
        <div class="row">
            <div class="col-sm-7">
           @foreach(App\Customize::where('type',7)->get() as $cust) 
                <h4>{!! model_translate($cust,'title') !!}</h4>
                <div class="clearfix"></div>
                <p>{!! model_translate($cust,'content') !!}</p>
           @endforeach
            </div>
            <div class="clearfix"></div>
            <div class="-wrapper-customer">
        
            @foreach(App\Work::orderby('id','DESC')->orderby('sort','ASC')->limit(8)->get() as $work)
                <div class="col-sm-3">
                    <div class="-cu-item">
                        <img class="-cu-img" src="{{ url('elixir/frontend/images/Jones-Harrison-thumbnail.png') }}" alt="" />
                        <div class="-cu-title">
                            <span>{!! model_translate($work,'title') !!}</span>
                            <i class="glyphicon glyphicon-option-vertical -open"></i>
                        </div>
                        <div class="-cu-panel">
                            <i class="fa fa-close -close"></i>

                            <span>{!! model_translate($work,'title') !!}</span>
                            <div class="clearfix"></div>
                            <p>{!! model_translate($work,'content') !!}</p>
                            <ul class="-cu-links">
                               <li>
                                   <a href="{!! $work->bs !!}" target="_blank"><i class="fa fa-behance"></i></a>
                               </li>
                               <li>
                                   <a href="{!! $work->link !!}" target="_blank"><i class="fa fa-external-link"></i></a>
                               </li>
                               <li>
                                   <a href="#" target="_blank"><i class="fa fa-eye"></i></a>
                               </li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endforeach
            @if(App\Work::count() > 9)
                <div class="col-sm-2 col-sm-offset-5">
                    <a href="{{ url('works') }}" class="btn  waves-effect full-width-btn fix-button pull-right ">المزيد</a>
                </div>
            @endif

            </div>
        </div>
    </div>
</section>
<div class="clear"></div>