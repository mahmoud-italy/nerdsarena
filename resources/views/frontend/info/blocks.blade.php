<section class="_how-buld stepss ">
    <div class="container-fluid ">
        <div class="row">
            <div class="col-sm-3">
               @include('frontend/svg/f-step')
                <span class="-hint">@if(!$service3) {{ '' }} @else {!! model_translate($service3,'title') !!} @endif</span>
            </div>
            <div class="col-sm-3">
               @include('frontend/svg/s-step')
                <span class="-hint">@if(!$service4) {{ '' }} @else {!! model_translate($service4,'title') !!} @endif</span>
            </div>
            <div class="col-sm-3">
                @include('frontend/svg/step3')
                <span class="-hint">@if(!$service5) {{ '' }} @else {!! model_translate($service5,'title') !!} @endif</span>
            </div>
            <div class="col-sm-3">
                @include('frontend/svg/step4')
                <span class="-hint">@if(!$service6) {{ '' }} @else {!! model_translate($service6,'title') !!} @endif</span>
            </div>
        </div>
    </div>
</section>