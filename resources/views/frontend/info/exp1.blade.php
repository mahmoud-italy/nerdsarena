
<section class="_serv">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <div class="-serv-right-content">
                    <h4>@if(!$service1) {{ '' }} @else {!! model_translate($service1,'title') !!} @endif</h4>
                    <p>@if(!$service1) {{ '' }} @else {!! model_translate($service1,'content') !!} @endif</p>
                    <a href="{{ url('services') }}" class="btn -border-style waves-effect fix-button">{{ lang('customize.services') }}</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="-serv-left-content">
                    <div class="clearfix"></div>
                </div>
                <div class="_serv-section remove-margin">
                    <div class="-bag ">
                        <div class="-bag-image ">
                            <!--                           start back bag animation-->
                            <img class="-back-bag" src="{{ url('elixir/frontend/images/back-bag.png') }}" alt="bag">
                            <!--                           start front bag animation-->
                            <img class="-front-bag" src="{{ url('elixir/frontend/images/front-bag.png') }}" alt="bag">
                            <!--                            start icon-->
                            <div class="item-rotate ">
                                <img class="-icon-1  -icon-0 -animate-option" src="{{ url('elixir/frontend/images/icon-1.png') }}" alt="icon">
                                <img class="-icon-2 -animate-option" src="{{ url('elixir/frontend/images/icon-2.png') }}" alt="icon">
                                <img class="-icon-3 -animate-option" src="{{ url('elixir/frontend/images/icon-3.png') }}" alt="icon">
                                <img class="-icon-4 -animate-option" src="{{ url('elixir/frontend/images/icon-4.png') }}" alt="icon">
                                <div class="right-logo-area">
                                    <img class="-icon-2 -animate-option" src="{{ url('elixir/frontend/images/icon-2.png') }}" alt="icon">
                                    <div class="bottom-logo-area  ">
                                        <img class="-icon-2 -animate-option " src="{{ url('elixir/frontend/images/icon-2.png') }}" alt="icon">
                                    </div>
                                    <img class="-icon-3 -animate-option" src="{{ url('elixir/frontend/images/icon-3.png') }}" alt="icon">
                                    <img class="-icon-4 -animate-option" src="{{ url('elixir/frontend/images/icon-4.png') }}" alt="icon">
                                </div>
                                <div class="left-logo-area">
                                    <img class="-icon-1 -icon-0 -animate-option" src="{{ url('elixir/frontend/images/icon-1.png') }}" alt="icon">
                                    <div class="top-logo-area">
                                        <img class="-icon-1 -icon-0 -animate-option" src="{{ url('elixir/frontend/images/icon-1.png') }}" alt="icon">
                                    </div>
                                    <img class="-icon-3 -animate-option" src="{{ url('elixir/frontend/images/icon-3.png') }}" alt="icon">
                                    <img class="-icon-4 -animate-option" src="{{ url('elixir/frontend/images/icon-4.png') }}" alt="icon">
                                </div>

                            </div>

                            <!--   <img class="-icon-5 -animate-option" src="images/icon-5.png" alt="icon"/>-->
                            <div class="-icon-5 -animate-option"></div>
                        </div>

                    </div>

                </div>
                <div class="animate-infograph _serv-section remove-margin-top">
                    <div class="col-sm-4 col-xs-6 -info-animate1">
                        <div class="infograph">
                            <img src="{{ url('elixir/frontend/images/web.png') }}" alt="" />
                        </div>
                        <a href="#" class="btn -border-style waves-effect services-btn">{{ lang('customize.web') }}</a>  </div>

                    <div class="col-sm-4 col-xs-6 -info-animate2">
                        <div class="infograph">
                            <img src="{{ url('elixir/frontend/images/1-2.png') }}" alt="" />
                        </div>
                        <a href="#" class="btn -border-style waves-effect services-btn"> {{ lang('customize.mobile') }}</a>  </div>

                    <div class="   col-xs-12 col-sm-4  -info-animate3 ">
                        <div class="infograph">
                            <img src="{{ url('elixir/frontend/images/code.png') }}" alt="" />
                        </div>
                        <a href="#" class="btn -border-style waves-effect services-btn">{{ lang('customize.programming') }}</a>  </div>
                </div>
            </div>
        </div>
    </div>

</section>
<div class="clearfix">

</div>