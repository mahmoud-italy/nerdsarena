@foreach(App\Seo::get() as $seo)
  <meta name="description" content="{{{ model_translate($seo,'meta_desc') }}}">
  <meta name="author" content="{{{ model_translate($seo,'meta_author') }}}">
  <meta name="keywords" content="{{{ model_translate($seo,'meta_key') }}}">
  <meta name="title" content="{{{ model_translate($seo,'meta_title') }}}">
@endforeach