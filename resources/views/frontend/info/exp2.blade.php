<section class="_how-buld">
    <div class="container-fluid ">
        <div class="row">
            <div class="col-sm-6 -right-side">
            @include('frontend/svg/pc')
            </div>
            <div class="col-sm-6 -left-side">
                <h4>@if(!$service2) {{ '' }} @else {!! model_translate($service2,'title') !!} @endif</h4>
                <p>@if(!$service2) {{ '' }} @else {!! model_translate($service2,'content') !!} @endif</p>
            </div>
        </div>
    </div>
  </section>