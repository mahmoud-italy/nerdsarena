<section class="_slider ">
  <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
      <ol class="carousel-indicators">
          <li data-target="#carousel" data-slide-to="0" class="active"></li>
          <li data-target="#carousel" data-slide-to="1"></li>
          <li data-target="#carousel" data-slide-to="2"></li>
          <li data-target="#carousel" data-slide-to="3"></li>
      </ol>

 <div class="carousel-inner">

  <div class="item active">
    <div class="-firist-slide-item margin-top">

      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <div class="-item-content">
              <h3>@if(!$slider1) {{ '' }} @else {!! model_translate($slider1,'title') !!} @endif</h3>
            </div>
          </div>

      <div class="col-sm-6">
        <div class=" slide-scal animates">

          @include('frontend/svg/responsive')

        </div>
      </div>

     </div>
    </div>
   </div>
  </div>
  

  <div class="item">
   <div class="clearfix"></div>
     <div class="-firist-slide-item margin-top">
      <div class="container">
       <div class="row">
    
    <div class="col-sm-6 col-xs-12">
        <div class="-item-content">
          <h3>@if(!$slider2) {{ '' }} @else {!! model_translate($slider2,'title') !!} @endif</h3>
        </div>
    </div>

     <div class="col-sm-6 col-xs-12">
      <div class="items-svg ">
           @include('frontend/svg/new-web')
       </div>
      </div>

     </div>
    </div>
   </div>
  </div>


  <div class=" item">
   <div class="clearfix"></div>
    <div class="-firist-slide-item margin-top">
     <div class="container">
       <div class="row">
    
    <div class="col-sm-6">
      <div class="-item-content">
        <h3>@if(!$slider3) {{ '' }} @else {!! model_translate($slider3,'title') !!} @endif</h3>
      </div>
    </div>

    <div class="col-xs-6  col-sm-6   ">
      <div class="items-svg">
      @include('frontend/svg/magic')
     </div>
    </div>

    </div>
   </div>
 </div>
</div>


 <div class="item">
  <div class="-firist-slide-item margin-top">
    <div class="container">
      <div class="row">

        <div class="col-sm-6">
          <div class="-item-content">
            <h3>@if(!$slider4) {{ '' }} @else {!! model_translate($slider4,'title') !!}  @endif</h3>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="-editor">
            <br>
          <span class="line"></span>                                                        </div>
        </div>
     </div>
    </div>
   </div>
  </div>


      </div>
  </div>
</section>
