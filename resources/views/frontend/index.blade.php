@extends('frontend.layout')
@section('content')

@include('frontend.info.slider')

@include('frontend.info.exp1')

@include('frontend.info.exp2')

@include('frontend.info.blocks')

@include('frontend.info.works')

@include('frontend.info.videos')

@include('frontend.info.clients')

@stop