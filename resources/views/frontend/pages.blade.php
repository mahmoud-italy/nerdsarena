@extends('frontend.layout')
@section('content')

<section class="_service">
  <div class="container">
   <div class="row">
     <article class="-heade-of-page">
        <h4>{!! model_translate($row,'name') !!}</h4>
      <div class="col-sm-8 col-sm-offset-2">
       <div class="form-wrapper">
         <p>{!! model_translate($row,'content') !!}</p>
       </div>
      </div>
    </article>
  </div>
 </div>
</section>       


@stop