@extends('frontend.layout')
@section('content')

<section class="_service">
  <div class="container">
   <div class="row">
     <article class="-heade-of-page">
       <div class="col-sm-8 col-sm-offset-2">
       
         @foreach(App\Customize::where('type',7)->get() as $cust) 
            <h4>{!! model_translate($cust,'title') !!}</h4>
            <p class="text-center work-p">{!! model_translate($cust,'content') !!}</p>
         @endforeach

      </div>

    <div class="form-wrapper">
      <section class="-custumer">
        <div class="clearfix"></div>
          <div class="-wrapper-customer">

@foreach(App\Work::OrderBy('sort','ASC')->paginate(24) as $row)
  <div class="col-sm-3">
      <div class="-cu-item">
          <img class="-cu-img" src="{!! url($row->image) !!}" alt="{!! $row->title !!}">
          <div class="-cu-title">
              <span>{!! model_translate($row,'title') !!}</span>
              <i class="glyphicon glyphicon-option-vertical -open"></i>
          </div>
          <div class="-cu-panel" style="height: 256px;">
              <i class="fa fa-close -close"></i>
              <span>{!! model_translate($row,'title') !!}</span>
              <div class="clearfix"></div>
              <p>{!! model_translate($row,'content') !!}</p>
              <ul class="-cu-links">
                  <li>
                      <a href="{!! $row->bs !!}" target="_blank"><i class="fa fa-behance"></i></a>
                  </li>
                  <li>
                      <a href="{!! $row->link !!}" target="_blank"><i class="fa fa-external-link"></i></a>
                  </li>
                  <li>
                      <a href="#" target="_blank"><i class="fa fa-eye"></i></a>
                  </li>
              </ul>
          </div>
      </div>
  </div>
@endforeach
<br/>
<div class="col-md-12"><br/>
  <center>{!! App\Work::OrderBy('sort','ASC')->paginate(24)->render() !!}</center>
</div>

       </div>
     </section>
   </div>
  </div>
 </div>
</section>

@stop