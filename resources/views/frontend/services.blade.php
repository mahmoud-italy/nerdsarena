@extends('frontend.layout')
@section('content')

<section class="_service">
    <div class="container">
        <div class="row">
            <article class="-heade-of-page">
                <h4>{{ lang('customize.services') }}</h4>
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="form-wrapper">

@if($errors->all())                        
 <ul class="alert alert-danger padding-left-30" style="text-align:left;"> 
   @foreach($errors->all() as $error)
     <li>{{ $error }}</li>
   @endforeach
 </ul>
@endif        
        {!! Form::Open(['id'=>'services','files'=>true]) !!}

                            <div class="input-field col-sm-6">
                                <input  id="name" name="name" type="text" value="{{{ Input::old('name') }}}" class="validate">
                                <label for="name">{{ lang('customize.name') }}</label>
                            </div>

                            <div class="input-field col-sm-6">
                                <input id="email-f" name="email" type="email" value="{{{ Input::old('email') }}}"  class="validate">
                                <label for="email-f">{{ lang('customize.email') }}</label>
                            </div>
                            <div class="input-field col-sm-6">
                                <input id="tel" name="phone" type="tel" value="{{{ Input::old('phone') }}}"  class="validate">
                                <label for="tel">{{ lang('customize.phone') }}</label>
                            </div>
                            <div class="input-field col-sm-6">
                                <select name="services">
                                    <option  disabled selected> {{ lang('customize.choice_serv') }}</option>
                                    <option value="Programming"
                                    @if(Input::old("services") == "Programming")
                                    selected="selected"
                                    @endif> {{ lang('customize.programming') }}</option>
                                    <option value="Mobile Application"
                                    @if(Input::old("services") == "Mobile Application")
                                    selected="selected"
                                    @endif> {{ lang('customize.mobile') }} </option>
                                    <option value="Web Application"
                                    @if(Input::old("services") == "Web Application")
                                    selected="selected"
                                    @endif>{{ lang('customize.web') }}</option>
                                </select>
                            </div>
                            <div class="clear">

                            </div>
                            <div class="input-field col-sm-6">
                                <div class="hints-">
                                    {{ lang('customize.choice_colors') }}
                                </div>
                                <div class="clear"></div>
                                <br />
                                <input class="color-pics " type="color" value="#414141"/>
                                <div class="clear"></div>
                                <br />
                                <div class="color-box">
                                    <span>{{ lang('customize.your_colors') }}</span>
                                    <div class="clear"></div>
                                    <br>
                                    <ul class="list-inline color-picks">
                                       
                                    </ul>
                                </div>
                            </div>
                            <div class="input-field col-sm-6">
                                <div class="hints-">
                                    {{ lang('customize.upload_details') }}
                                </div>
                                <div class="file-field input-field">
                                    <div class="btn fix-button pull-right">
                                        <span>{{ lang('customize.upload_file') }} <i class="material-icons left"></i></span>
                                        <input type="file" id="upload_file" name="file"> 
                                         <br/><br/><br/>
                                        <img id="loading" src="{{ url('elixir/frontend/img/loading.gif') }}" style="display:none;width:25px;height:25px;">
                                        <img id="success" src="{{ url('elixir/frontend/img/success.jpg') }}" style="display:none;width:25px;height:25px;" data-toggle="tooltip" title="تم رفم الملف بنجاح">
                                        <input type='hidden' name="file_uploaded" id="file_uploaded">
                                    </div>
                                    <div class="file-path-wrapper display-none">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="clear">

                            </div>


                            <div class="margin-top-50">

                                <div class="input-field col-sm-10 col-sm-offset-1  ">
                                    <textarea id="textarea1" class="materialize-textarea" name="message">{{{ Input::old('message') }}}</textarea>
                                    <label for="textarea1">{{ lang('customize.your_msg') }}</label>
                                </div>
                            </div>
                            <button class="btn waves-effect waves-light blue col-sm-offset-5" type="submit" name="action">{{ lang('customize.send') }}
                                <i class="material-icons right"></i>
                            </button>
                            
        {!! Form::Close() !!}

                    </div>

                </div>
            </article>
        </div>
    </div>
</section>


@stop


@section('jsCode')

<script>

$(document).ready(function(){
/* Upload File */
$('#upload_file').on('change', function() {
  var fileExtension = ['pdf', 'jpg','jpeg','png','docs','docx'];
  if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
    sweetAlert('...Error','من فضلك ارفع ملف PDF , JPG , DOCX فقط ','error');
    $('#upload_file').val('');
    hide_loading();
  }
  else
  {upload_file();}
});

$.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') }
    });

function upload_file(){       
  var form = $('#services');
  var file_data = $('#upload_file').prop('files')[0]; 
  var file_name = $('#upload_file').val();
  var file_ext =  file_name.replace(/^.*\./, '');
  var form_data = new FormData(); 
  form_data.append('file', file_data);
  form_data.append('ext', file_ext);   
  var Url = '{{ url("services/upload_file") }}';

  show_loading();

  

  $.ajax({
        url : Url,
        type : "POST",
        data : form_data,
        dataType : "json",
        cache: false,
        contentType: false,
        processData: false,
        success : function(data){
            if(data.success){
                $('#file_uploaded').val(data.upload_file);
                $('#upload_file').val('');
                show_success();
            }
            else {
                hide_loading();
            }
        }
    });
    return false;
};

/* Loading Upload */
function show_loading(){
  $('#loading').css("display","block");
  $('#success').css("display","none");
};
function hide_loading(){
  $('#loading').css("display","none");
  $('#success').css("display","none");
};
function show_success(){
  $('#loading').css("display","none");
  $('#success').css("display","block");
};

});

</script>

@if(Session::has('success'))
<script>
 sweetAlert('...Done','{{ lang("customize.serv_msg_success") }}','success');
</script>
@elseif(Session::has('error'))
<script>
 sweetAlert('...Oops','{{ lang("customize.serv_msg_error") }}','error');
</script>
@endif

<script>
$(document).ready(function(){
 $('.color-pics').change(function(){
  var val = $('.color-pics').val();
  $('.color-picks').append('<li onclick="remove(this);"><input type="text" name="colors[]" class="it-input-colors" style="color:'+val+';background:'+val+';" value="'+val+'"></li>');
   });
});
</script>

@stop

