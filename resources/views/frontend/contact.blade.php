@extends('frontend.layout')
@section('content')

<section class="_service">
    <div class="container">
        <div class="row">
            <article class="-heade-of-page">
                <h4>{{ lang('customize.contact_us') }}</h4>
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="form-wrapper">

@if($errors->all())                        
 <ul class="alert alert-danger padding-left-30" style="text-align:left;"> 
   @foreach($errors->all() as $error)
     <li>{{ $error }}</li>
   @endforeach
 </ul>
@endif

        {!! Form::Open(['id'=>'contact']) !!}

                            <div class="input-field col-sm-6">
                                <input id="name" type="text" name="name" value="{{{ Input::old('name') }}}" class="validate">
                                <label for="name">{{ lang('customize.name') }}</label>
                            </div>

                            <div class="input-field col-sm-6">
                                <input id="email-f" type="email" name="email" value="{{{ Input::old('email') }}}" class="validate">
                                <label for="email-f">{{ lang('customize.email') }}</label>
                            </div>
                            <div class="input-field col-sm-6">
                                <input id="tel" type="tel" name="phone" value="{{{ Input::old('phone') }}}" class="validate">
                                <label for="tel">{{ lang('customize.phone') }}</label>
                            </div>
                            <div class="input-field col-sm-6">
                                <input id="sub" type="text" name="title" value="{{{ Input::old('title') }}}" class="validate">
                                <label for="sub"> {{ lang('customize.msg_title') }}</label>
                            </div>

                            <div class="margin-top-50">

                                <div class="input-field col-sm-10 col-sm-offset-1  ">
                                    <textarea id="textarea1" class="materialize-textarea" name="message" >{{{ Input::get('message') }}}</textarea>
                                    <label for="textarea1">{{ lang('customize.your_msg') }}</label>
                                </div>
                            </div>
                            <button class="btn waves-effect waves-light blue col-sm-offset-5" type="submit" name="action">{{ lang('customize.send') }}
                                <i class="material-icons right"></i>
                            </button>
    
    {!! Form::Close() !!}

                    </div>

    </div>
   </div>
  </div>
</section>


@stop

@section('jsCode')

 @if(Session::has('success'))
 <script>
   sweetAlert('...Done','{{ lang("customize.success_msg") }}','success');
 </script>
 @elseif(Session::has('error'))
 <script>
  sweetAlert('...Oops','{{ lang("customize.error_msg") }}','error');
 </script>
 @endif

@stop