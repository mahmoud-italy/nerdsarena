<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="MobileOptimized" content="320" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="{{ url('elixir/frontend/login/bootstrap-rtl.css') }}" rel="stylesheet">
    <link href="{{ url('elixir/frontend/login/style-rtl.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url('elixir/frontend/sweetAlert/sweetalert.css') }}">
</head>
<body class="login-screen">
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="login-box">
                    <div class="login-content">
                        <div class="login-user-icon">
                            <i class="glyphicon glyphicon-user"></i>

                        </div>
                        <h3>Identify Yourself</h3>
                        <div class="social-btn-login">

                        </div>
                    </div>

                    <div class="login-form" style='direction:ltr;'>
                    {!! Form::Open() !!}
                            <div class="input-group ls-group-input">
                                <input class="form-control" type="email" name="email" placeholder="Email">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            </div>

                            <div class="input-group ls-group-input">
                                <input type="password" placeholder="Password" name="password"
                                       class="form-control">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            </div>
                            <div class="input-group login-btn-box">
                                <button class="btn ls-dark-btn ladda-button col-md-12 col-sm-12 col-xs-12" >
                                    <span class="ladda-label"><i class="fa fa-key"></i> Login </span>
                                </button>

                            </div>
                      {!! Form::Close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript" src="{{ url('elixir/frontend/sweetAlert/sweetalert.min.js') }}"></script>                        
 
@if($errors->all())  
<script>
sweetAlert('...Oops','Empty Value','error');
</script> 
@elseif(Session::has('error_login')) 
<script>
sweetAlert('...Oops','Incorrect Email or Password','error');
</script>
@endif

</body>
</html>