<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Nerds Arena</title>

  @include('frontend.info.seo')
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ url('elixir/frontend/css/materialize-rtl.css') }}"/>
    <link rel="stylesheet" href="{{ url('elixir/frontend/css/bootstrap.css') }}"/>

  @if(App::getLocale() == 'ar')
    <link rel="stylesheet" href="{{ url('elixir/frontend/css/bootstrap-rtl.min.css') }}"/>
  @endif

    <link rel="stylesheet" href="{{ url('elixir/frontend/fonts/droid.css') }}"/>
    <link rel="stylesheet" href="{{ url('elixir/frontend/fonts/font.css') }}"/>
    <link rel="stylesheet" href="{{ url('elixir/frontend/fonts/font2.css') }}"/>
    <link rel="stylesheet" href="{{ url('elixir/frontend/css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" href="{{ url('elixir/frontend/css/jquery.bxslider.css') }}"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('elixir/frontend/css/style.css') }}"/>
    <link rel="stylesheet" href="{{ url('elixir/frontend/css/svg-animations.css') }}"/>
    <link rel="stylesheet" href="{{ url('elixir/frontend/css/key-frame.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('elixir/frontend/sweetAlert/sweetalert.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('elixir/frontend/css/my-style.css') }}">
    <link rel="shortcut icon" type="image/png" href="{{ url('elixir/frontend/img/favicon.png') }}"/>
   </head>
<body>

@if(Auth::check())
<nav class="it-cust-nav it-nav">
  <div class="pull-right">
     <a href="{{ url('dashboard') }}" class="it-cf it-mgl"><i class="fa fa-reply it-ico"></i></a> 
  </div>
</nav>
@endif

  <nav class="-navbar-top">
    <div class="-tap-button">
      <i class="material-icons right">dehaze</i>
    </div>
      <ul class="-menu">
        <div class="-tap-button">
          <i class="material-icons right">close</i>
        </div>
        <li><a href="{{ url('/') }}">{{ lang('nav.home') }}</a></li>
        <li><a href="{{ url('services') }}">{{ lang('nav.services') }}</a></li>
        <li><a href="{{ url('works') }}">{{ lang('nav.works') }}</a></li>
        <li><a href="{{ url('services') }}">{{ lang('nav.ask_work') }}</a></li>
        <li><a href="{{ url('about') }}">{{ lang('nav.about') }}</a></li>
        <li><a href="{{ url('contact') }}">{{ lang('nav.contact') }}</a></li>
 
      @if(App::getLocale() == 'ar')  
        <li><a href="{{ url('/lang/en') }}">English</a></li>
      @else
        <li><a href="{{ url('/lang/ar') }}">عربي</a></li>
      @endif

      </ul>

      <div class="-logo">
        <a href="{{ url('/') }}"><img src="{{ url('elixir/frontend/images/logo.png') }}" alt="Nerds Arena"></a>
      </div>

  </nav>



@yield('content')




@include('frontend.info.footer')

  <div class="-rights">{{ lang('customize.copyright') }}</div>



<script src="{{ url('elixir/frontend/js/jquery-1.11.2.min.js') }}"></script>
<script src="{{ url('elixir/frontend/js/TweenMax.min.js') }}"></script>
<script src="{{ url('elixir/frontend/js/jquery.bxslider.js') }}"></script>

<script src="{{ url('elixir/frontend/js/materialize.min.js') }}"></script>
<script src="{{ url('elixir/frontend/js/bootstrap.js') }}"></script>
<script src="{{ url('elixir/frontend/js/typed.js') }}"></script>
<script src="{{ url('elixir/frontend/sweetAlert/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ url('elixir/frontend/js/script.js') }}"></script>





<script>

$(window).load(function(){
  $('.bxslider').bxSlider({
  infiniteLoop: false,
  minSlides: 1,
  maxSlides:10,
  slideWidth: 170,
  slideMargin: 5,
  ticker: true,
  useCSS:false,
  speed: 20000
 });
});

</script>

<script>
  $('.carousel').carousel();

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>



@yield('jsCode')




</html>
</body>

