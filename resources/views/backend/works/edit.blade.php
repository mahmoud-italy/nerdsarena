@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Works</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Update Work</h3>
      </div>
   
        <div class="panel-body">

@if($errors->all())                        
 <ul class="alert alert-danger padding-left-30"> 
   @foreach($errors->all() as $error)
     <li>{{ $error }}</li>
   @endforeach
 </ul>
@endif

      {!! Form::Open(['files'=>true]) !!}

           <div class="col-md-6">

             <div class="form-group">
               <label>Title</label>
               <input type="text" name="title_en" class="form-control" value="{{ $row->title_en }}" required>
             </div>

             <div class="form-group">
               <label>Content</label>
               <textarea rows="7" name="content_en" class="form-control">{{ $row->content_en }}</textarea>
             </div>

             <div class="form-group">
               <label>Image</label>
               <img id="img" src="{{ url($row->image) }}" class="w-img">
               <input type="file" data-image-target="#img" name="image" accept="image/jpeg,jpg,png">
             </div>

            
             <div class="form-group">
             <br/>
              <button class="btn btn-primary">Update</button>
              <a onclick="history.go(-1);" class="btn btn-danger"> Back</a>
             </div>

           </div>
           <div class="col-md-6">

             <div class="form-group">
               <label class="ar-lbl">( عربي ) Title</label>
               <input type="text" name="title" class="form-control ar-float" value="{{ $row->title }}" required>
             </div>

             <div class="form-group">
               <label class="ar-lbl">( عربي ) Content</label>
               <textarea rows="7" name="content" class="form-control ar-float">{{ $row->content }}</textarea>
             </div>

              <div class="form-group">
               <label>Link</label>
               <input type="text" name="link" class="form-control" value="{{ $row->link }}">
             </div>

             <div class="form-group">
               <label>Behance</label>
               <input type="text" name="bs" class="form-control" value="{{ $row->bs }}">
             </div>


             <div class="form-group">
               <label>Sort</label>
               <input type="number" name="sort" value="{{ $row->sort }}" class="form-control">
             </div>

             


           </div>

      
      {!! Form::Close() !!}
        </div>

   </div>
  </div>
</div>

@stop

@section('jsCode')

<script>
function readURL(input) {
    if (input.files && input.files[0]) {
        var image = jQuery(input).data('image-target');

        var reader = new FileReader();

        reader.onload = function (e) {
            jQuery(image).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
jQuery("input[type=file]").change(function() {
   readURL(this);
});
</script>

@stop