@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Customize</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Add Customize</h3>
      </div>
   
        <div class="panel-body">

@if($errors->all())                        
 <ul class="alert alert-danger padding-left-30"> 
   @foreach($errors->all() as $error)
     <li>{{ $error }}</li>
   @endforeach
 </ul>
@endif

      {!! Form::Open() !!}

           <div class="col-md-6">

             <div class="form-group">
               <label>Title</label>
               <input type="text" name="title_en" class="form-control" required>
             </div>

             <div class="form-group">
               <label>Content</label>
               <textarea rows="7" name="content_en" class="form-control"></textarea>
             </div>

             <div class="form-group">
               <label>Type</label>
               <select name="type" class="form-control">
                 <option value="1">Section 1</option>
                 <option value="2">Section 2</option>
                 <option value="3">Block 1</option>
                 <option value="4">Block 2</option>
                 <option value="5">Block 3</option>
                 <option value="6">Block 4</option>
                 <option value="7">Our Works</option>
               </select>
             </div>

             <div class="form-group">
             <br/>
              <button class="btn btn-primary">Submit</button>
              <a onclick="history.go(-1);" class="btn btn-danger"> Back</a>
             </div>

           </div>
           <div class="col-md-6">

             <div class="form-group">
               <label class="ar-lbl">( عربي ) Title</label>
               <input type="text" name="title" class="form-control ar-float" required>
             </div>

             <div class="form-group">
               <label class="ar-lbl">( عربي ) Content</label>
               <textarea rows="7" name="content" class="form-control ar-float"></textarea>
             </div>


           </div>

      
      {!! Form::Close() !!}
        </div>

   </div>
  </div>
</div>

@stop
