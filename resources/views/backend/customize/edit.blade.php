@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Customize</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Update Customize</h3>
      </div>
   
        <div class="panel-body">

@if($errors->all())                        
 <ul class="alert alert-danger padding-left-30"> 
   @foreach($errors->all() as $error)
     <li>{{ $error }}</li>
   @endforeach
 </ul>
@endif

      {!! Form::Open() !!}

           <div class="col-md-6">

             <div class="form-group">
               <label>Title</label>
               <input type="text" name="title_en" class="form-control" value="{{ $row->title_en }}" required>
             </div>

             <div class="form-group">
               <label>Content</label>
               <textarea rows="7" name="content_en" class="form-control">{{ $row->content_en }}</textarea>
             </div>

             <div class="form-group">
               <label>Type</label>
               <select name="type" class="form-control">
                 <option value="1"
                 @if($row->type == 1)
                 selected="selected"
                 @endif>Section 1</option>
                 <option value="2"
                 @if($row->type == 2)
                 selected="selected"
                 @endif>Section 2</option>
                 <option value="3"
                 @if($row->type == 3)
                 selected="selected"
                 @endif>Block 1</option>
                 <option value="4"
                 @if($row->type == 4)
                 selected="selected"
                 @endif>Block 2</option>
                 <option value="5"
                 @if($row->type == 5)
                 selected="selected"
                 @endif>Block 3</option>
                 <option value="6"
                 @if($row->type == 6)
                 selected="selected"
                 @endif>Block 4</option>
                 <option value="7"
                 @if($row->type == 7)
                 selected="selected"
                 @endif>Our Works</option>
               </select>
             </div>

             <div class="form-group">
             <br/>
              <button class="btn btn-primary">Update</button>
              <a onclick="history.go(-1);" class="btn btn-danger"> Back</a>
             </div>

           </div>
           <div class="col-md-6">

             <div class="form-group">
               <label class="ar-lbl">العنوان</label>
               <input type="text" name="title" class="form-control ar-float" value="{{ $row->title }}" required>
             </div>

             <div class="form-group">
               <label class="ar-lbl">المحتوي</label>
               <textarea rows="7" name="content" class="form-control ar-float">{{ $row->content }}</textarea>
             </div>


           </div>

      
      {!! Form::Close() !!}
        </div>

   </div>
  </div>
</div>

@stop
