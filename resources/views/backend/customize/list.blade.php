@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Customize</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">All Customize ( {{ count($data) }} )</h3>
      </div>
   
        <div class="panel-body">
        
        <a href="{{ url('dashboard/customize/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add Customize</a><br/><br/>

@if(Session::has('success'))
 <p class="alert alert-success">{{ Session::get('success') }}</p>
@elseif(Session::has('error'))
 <p class="alert alert-danger">{{ Session::get('error') }}</p>
@endif
          <div class="ls-editable-table table-responsive ls-table">
            <table class="table table-bordered table-striped table-bottomless" id="ls-editable-table">
             <thead>
               <tr>
                   <th class="f-row">#</th>
                   <th>Title <span class="ar">( عربي )</span></th>
                   <th>Title ( English )</th>
                   <th class="t-date">Type</th>
                   <th class="f-action">Action</th>
               </tr>
             </thead>
             <tbody>
          @foreach($data as $key => $row)   
             <tr>
                 <td class="f-row">
                   @if($key == 0)
                    {{ '1' }}
                   @else
                    {{ $key+1 }}
                   @endif
                 </td>
                 <td class="ar">{{ $row->title }}</td>
                 <td>{{ $row->title_en }}</td>
                 <td class="t-date">
                 @if($row->type == 1)
                  {{ 'Section 1' }}
                 @elseif($row->type == 2)
                  {{ 'Section 2' }}
                 @elseif($row->type == 3)
                  {{ 'Block 1' }}
                 @elseif($row->type == 4)
                  {{ 'Block 2' }}
                 @elseif($row->type == 5)
                  {{ 'Block 3' }}
                 @elseif($row->type == 6)
                  {{ 'Block 4' }}
                 @elseif($row->type == 7)
                  {{ 'Our Works' }}     
                 @endif
                 </td>
                 <td class="f-action">
              {!! Form::Open(['url'=>'dashboard/customize/del/'.$row->id]) !!}   
                 	<a href="{{ url('dashboard/customize/edit/'.$row->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                  <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
              {!! Form::Close() !!}
                 </td>
             </tr>
          @endforeach
             </tbody>
            </table>
          </div>
        </div>

   </div>
  </div>
</div>

@stop