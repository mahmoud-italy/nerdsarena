@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Messages</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Read Message</h3>
      </div>
      
      <div class="panel-body">

@if($errors->all())                        
 <ul class="alert alert-danger padding-left-30"> 
   @foreach($errors->all() as $error)
     <li>{{ $error }}</li>
   @endforeach
 </ul>
@endif

@if(Session::has('success'))
<p class="alert alert-success">{{ Session::get('success') }}</p>
@elseif(Session::has('error'))
<p class="alert alert-danger">{{ Session::get('error') }}</p>
@endif

        

      {!! Form::Open() !!}
       <div class="col-md-3 pull-left t-left">
        <p> <b>Name : </b>{{ $row->name }}</p>
        <p> <b>Email : </b>{{ $row->email }}</p>
        <p> <b>Phone : </b>{{ $row->phone }}</p>
       </div>
           <div class="col-md-12">
           <hr>
                <center><h3>{{ $row->title }}</h3></center>
                <p>{{ $row->message }}</p>
           </div>

           <div class="col-md-12">
             <hr><br/>
            <h4>Reply ( {{App\Contact_reply::where('msg_id',$row->id)->count()}} ) </h4>
            <br/>
           </div>
    @if(App\Contact_reply::where('msg_id',$row->id)->count() > 0)     
        @foreach(App\Contact_reply::where('msg_id',$row->id)->get() as $reply)
          <div class="col-md-12">
             <p><b>From : </b>Support@nerdsarena.com</p>
             <p><b>Message : </b>{{ $reply->content }}</p>
             <hr>
          </div>
        @endforeach
    @endif

           <div class="col-md-12">
             {!! Form::Open() !!}
            <br/><br/>
             <div class="form-group">
               <textarea class="form-control" rows="7" name="reply" required></textarea>
             </div>
             <div class="form-group">
               <button class="btn btn-primary" disabled>Reply</button>
               <a onclick="history.go(-1);" class="btn btn-danger">Back</a>
             </div>

             {!! Form::Close() !!}
           </div>

      {!! Form::Close() !!}
        </div>

   </div>
  </div>
</div>

@stop
