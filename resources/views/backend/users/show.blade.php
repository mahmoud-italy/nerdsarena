@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Users</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Show {!! $row->name !!}</h3>
      </div>
   
        <div class="panel-body">

           <div class="col-md-6">

             <div class="form-group">
               <label>Name</label>
               <input type="text" readonly class="form-control rd-only" value="{!! $row->name !!}">
             </div>

             <div class="form-group">
               <label>Position</label>
               <input type="text" readonly class="form-control rd-only" value="{!! $row->position !!}">
             </div>

             <div class="form-group">
               <label>Email</label>
               <input type="email" readonly class="form-control rd-only" value="{!! $row->email !!}">
             </div>

             <div class="form-group">
               <label>Password</label>
               <input type="password" readonly class="form-control rd-only" placeholder="**********">
             </div>

             <div class="form-group">
              <a onclick="history.go(-1);" class="btn btn-danger"> Back</a>
             </div>

           </div>

           <div class="col-md-4">

             <div class="form-group">
               <label>Image</label>
               <img id="image" src="@if(!empty($row->image)) {{ url($row->image) }} @else {{ url('uploads/users/whois.png') }} @endif" class="img-view-th-ds">
             </div>

           </div>
      
        </div>

   </div>
  </div>
</div>

@stop
