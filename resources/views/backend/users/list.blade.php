@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Users</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">All Users ( {{ count($users) }} )</h3>
      </div>
   
        <div class="panel-body">
        
        <a href="{{ url('dashboard/users/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add User</a><br/><br/>

@if(Session::has('success'))
 <p class="alert alert-success">{{ Session::get('success') }}</p>
@elseif(Session::has('error'))
 <p class="alert alert-danger">{{ Session::get('error') }}</p>
@endif
          <div class="ls-editable-table table-responsive ls-table">
            <table class="table table-bordered table-striped table-bottomless" id="ls-editable-table">
             <thead>
               <tr>
                   <th class="f-row">#</th>
                   <th class="img-row">Image</th>
                   <th>Name</th>
                   <th>Email</th>
                   <th>Position</th>
                   <th class="f-action-s">Action</th>
               </tr>
             </thead>
             <tbody>
          @foreach($users as $key => $row)   
             <tr>
                 <td class="f-row">
                   @if($key == 0)
                    {{ '1' }}
                   @else
                    {{ $key+1 }}
                   @endif
                 </td>
                 <td class="img-row"><img src="@if(!empty($row->image)) {{ url($row->image) }} @else {{ url('uploads/users/whois.png') }} @endif" class="img-view"></td>
                 <td>{{ $row->name }}</td>
                 <td>{{ $row->email }}</td>
                 <td>{{ $row->position }}</td>
                 <td class="f-action-s">
              {!! Form::Open(['url'=>'dashboard/users/del/'.$row->id]) !!}   
                  <a href="{{ url('dashboard/users/show/'.$row->id) }}" class="btn btn-success"><i class="fa fa-eye"></i></a>
              @if($row->id == Auth::user()->id || Auth::user()->position == 'Administrator')
              <a href="{{ url('dashboard/users/edit/'.$row->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
              @else
              <a href="#" disabled class="btn btn-primary"><i class="fa fa-edit"></i></a>
              @endif  
                @if($row->id == 1)   	
                  <button class="btn btn-danger" disabled><i class="fa fa-trash"></i></button>
                @elseif($row->id == Auth::user()->id || Auth::user()->position == 'Administrator')
                 <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
                @else
                  <button class="btn btn-danger" disabled><i class="fa fa-trash"></i></button>
                @endif
              {!! Form::Close() !!}
                 </td>
             </tr>
          @endforeach
             </tbody>
            </table>
          </div>
        </div>

   </div>
  </div>
</div>

@stop