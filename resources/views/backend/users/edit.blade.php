@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Users</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Update {{ $row->name }}</h3>
      </div>
   
        <div class="panel-body">

@if($errors->all())                        
 <ul class="alert alert-danger padding-left-30"> 
   @foreach($errors->all() as $error)
     <li>{{ $error }}</li>
   @endforeach
 </ul>
@endif

      {!! Form::Open(['files'=>true]) !!}

           <div class="col-md-6">

             <div class="form-group">
               <label>Name</label>
               <input type="text" name="name" class="form-control" value="{{ $row->name }}" required>
             </div>

             <div class="form-group">
               <label>Position</label>
               <input type="text" name="position" class="form-control" value="{{ $row->position }}" required>
             </div>

             <div class="form-group">
               <label>Email</label>
               <input type="email" name="email" class="form-control" value="{{ $row->email }}" required>
             </div>

             <div class="form-group">
               <label>Password</label>
               <input type="password" name="password" class="form-control" placeholder="**********">
             </div>

             <div class="form-group">
              <button class="btn btn-primary">Update</button>
              <a onclick="history.go(-1);" class="btn btn-danger"> Back</a>
             </div>

           </div>

           <div class="col-md-4">

             <div class="form-group">
               <label>Image</label>
               <img id="image" src="@if(!empty($row->image)) {{ url($row->image) }} @else {{ url('uploads/users/whois.png') }} @endif" class="img-view-th">
               <input type="file" data-image-target="#image" name="image" accept="image/jpeg,jpg,png">
             </div>

           </div>
      
      {!! Form::Close() !!}
        </div>

   </div>
  </div>
</div>

@stop

@section('jsCode')

<script>
function readURL(input) {
    if (input.files && input.files[0]) {
        var image = jQuery(input).data('image-target');

        var reader = new FileReader();

        reader.onload = function (e) {
            jQuery(image).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
jQuery("input[type=file]").change(function() {
   readURL(this);
});
</script>

@stop