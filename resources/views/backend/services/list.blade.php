@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Services</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">All Services ( {{ count($data) }} )</h3>
      </div>
   
        <div class="panel-body">
        
@if(Session::has('success'))
 <p class="alert alert-success">{{ Session::get('success') }}</p>
@elseif(Session::has('error'))
 <p class="alert alert-danger">{{ Session::get('error') }}</p>
@endif
          <div class="ls-editable-table table-responsive ls-table">
            <table class="table table-bordered  table-bottomless" id="ls-editable-table">
             <thead>
               <tr>
                   <th class="f-row">#</th>
                   <th>Name</th>
                   <th>Email</th>
                   <th class="t-date">Date</th>
                   <th class="f-action">Action</th>
               </tr>
             </thead>
             <tbody>
          @foreach($data as $key => $row) 
           @if($row->status == 0)
            <tr class="r-seen">
           @else
            <tr>
           @endif
                 <td class="f-row">
                   @if($key == 0)
                    {{ '1' }}
                   @else
                    {{ $key+1 }}
                   @endif
                 </td>
                 <td class="ar">{{ $row->name }} </td>
                 <td>{{ $row->email }}</td>
                  <td class="t-date">{{ explode(' ',$row->created_at)[0] }}</td>
                 <td class="f-action">
              {!! Form::Open(['url'=>'dashboard/services/del/'.$row->id]) !!}   
                 	<a href="{{ url('dashboard/services/reply/'.$row->id) }}" class="btn btn-primary"><i class="fa fa-reply"></i></a>
                  <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
              {!! Form::Close() !!}
                 </td>
             </tr>
          @endforeach
             </tbody>
            </table>
          </div>
        </div>

   </div>
  </div>
</div>

@stop