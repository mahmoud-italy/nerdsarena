@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Services</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Read Service</h3>
      </div>
      
      <div class="panel-body">

@if($errors->all())                        
 <ul class="alert alert-danger padding-left-30"> 
   @foreach($errors->all() as $error)
     <li>{{ $error }}</li>
   @endforeach
 </ul>
@endif

@if(Session::has('success'))
<p class="alert alert-success">{{ Session::get('success') }}</p>
@elseif(Session::has('error'))
<p class="alert alert-danger">{{ Session::get('error') }}</p>
@endif

        

      {!! Form::Open() !!}
       <div class="col-md-3 pull-left" style="text-align:left;">
        <p> <b>Name : </b>{!! $row->name !!}</p>
        <p> <b>Email : </b>{!! $row->email !!}</p>
        <p> <b>Phone : </b>{!! $row->phone !!}</p>
        <p> <b>Service Type :  </b>{!! $row->type !!}</p>
        <p> <b> File :</b> 
        @if(!empty($row->file))
        &nbsp;<a href="{!! $row->file !!}" download class="btn btn-primary"><i class="fa fa-download"></i> Download File</a>
        @else
         {{ 'Not Found' }}
        @endif
        </p>
        
        <p> <b>Colors : </b> 
        <ul class="list-inline">
       @if(App\Service_color::where('service_id',$row->id)->count() > 0) 
        @foreach( App\Service_color::where('service_id',$row->id)->get() as $color )
          <li style="width:15px;height:15px;border:1px solid {{ $color->colors }};background:{{ $color->colors }}"></li>
        @endforeach
       @else
         {{ 'Not Found' }}
       @endif
        </ul>
         </p> 
      </div>
           <div class="col-md-12">
           <hr>
                <center><h3>{!! $row->title !!}</h3></center>
                <p>{!! $row->message !!}</p>
           </div>

          <div class="col-md-12">
           <hr>
           <a href="history.go(-1)" class="btn btn-danger">Back</a>

    
        </div>

   </div>
  </div>
</div>

@stop
