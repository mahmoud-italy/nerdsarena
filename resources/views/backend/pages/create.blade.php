@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Pages</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Add Page</h3>
      </div>
   
        <div class="panel-body">

@if($errors->all())                        
 <ul class="alert alert-danger padding-left-30"> 
   @foreach($errors->all() as $error)
     <li>{{ $error }}</li>
   @endforeach
 </ul>
@endif

      {!! Form::Open() !!}

           <div class="col-md-6">

             <div class="form-group">
               <label>Name</label>
               <input type="text" id="page_name" name="name_en" value="{!! Input::old('name_en') !!}" class="form-control" required>
             </div>

             <div class="form-group">
               <label>Content</label>
               <textarea rows="7" name="content_en" class="form-control">{!! Input::old('content_en') !!}</textarea>
             </div>

             <div class="form-group">
               <label>Link</label>
              <div class="input-group">
               <span class="input-group-addon" id="basic-addon1">page/</span>
               <input type="text" id="page_link" name="link" aria-describedby="basic-addon1" value="{!! Input::old('link') !!}" class="form-control">
              </div>
             </div>

             <div class="form-group">
             <br/>
              <button class="btn btn-primary">Submit</button>
              <a onclick="history.go(-1);" class="btn btn-danger"> Back</a>
             </div>

           </div>
           <div class="col-md-6">

             <div class="form-group">
               <label class="ar-lbl">( عربي ) Name</label>
               <input type="text" name="name" value="{!! Input::old('name') !!}" class="form-control ar-float" required>
             </div>

             <div class="form-group">
               <label class="ar-lbl">( عربي ) Content</label>
               <textarea rows="7" name="content" class="form-control ar-float">{!! Input::old('content') !!}</textarea>
             </div>

             <div class="form-group">
               <label>Sort</label>
               <input type="number" name="sort" value="{!! Input::old('sort') !!}" class="form-control">
             </div>


           </div>

      
      {!! Form::Close() !!}
        </div>

   </div>
  </div>
</div>

@stop

@section('jsCode')

<script>
$(document).ready(function(){
  $('#page_name').keyup(function(){
    var name = $('#page_name').val();
    var regx = name.replace(/\s+/g, '-').toLowerCase();
   $('#page_link').val(regx);
  });
});
</script>

@stop