@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Sliders</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Update Slider</h3>
      </div>
   
        <div class="panel-body">

@if($errors->all())                        
 <ul class="alert alert-danger padding-left-30"> 
   @foreach($errors->all() as $error)
     <li>{{ $error }}</li>
   @endforeach
 </ul>
@endif

      {!! Form::Open() !!}

           <div class="col-md-6">

             <div class="form-group">
               <label>Title</label>
               <input type="text" name="title" class="form-control" value="{{ $row->title }}" required>
             </div>

             <div class="form-group">
               <label>Title ( English )</label>
               <input type="text" name="title_en" class="form-control" value="{{ $row->title_en }}" required>
             </div>

             <div class="form-group">
               <label>Type</label>
               <select name="type" class="form-control">
                <option value="1"
                @if($row->type ==1)
                selected="selected"
                @endif>Slider 1</option>
                <option value="2"
                @if($row->type ==2)
                selected="selected"
                @endif>Slider 2</option>
                <option value="3"
                @if($row->type ==3)
                selected="selected"
                @endif>Slider 3</option>
                <option value="4"
                @if($row->type ==4)
                selected="selected"
                @endif>Slider 4</option>
               </select>
             </div>

             <div class="form-group">
             <br/>
              <button class="btn btn-primary">Update</button>
              <a onclick="history.go(-1);" class="btn btn-danger"> Back</a>
             </div>

           </div>

      
      {!! Form::Close() !!}
        </div>

   </div>
  </div>
</div>

@stop
