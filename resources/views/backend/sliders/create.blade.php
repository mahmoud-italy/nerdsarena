@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Sliders</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Add Slider</h3>
      </div>
   
        <div class="panel-body">

@if($errors->all())                        
 <ul class="alert alert-danger padding-left-30"> 
   @foreach($errors->all() as $error)
     <li>{{ $error }}</li>
   @endforeach
 </ul>
@endif

      {!! Form::Open() !!}

           <div class="col-md-6">

             <div class="form-group">
               <label class="ar-lbl">( عربي ) Title</label>
               <input type="text" name="title" class="form-control ar-float" required>
             </div>

             <div class="form-group">
               <label>Title ( English )</label>
               <input type="text" name="title_en" class="form-control" required>
             </div>

             <div class="form-group">
               <label>Type</label>
               <select name="type" class="form-control">
                <option value="1">Slider 1</option>
                <option value="2">Slider 2</option>
                <option value="3">Slider 3</option>
                <option value="4">Slider 4</option>
               </select>
             </div>

             <div class="form-group">
             <br/>
              <button class="btn btn-primary">Submit</button>
              <a onclick="history.go(-1);" class="btn btn-danger"> Back</a>
             </div>

           </div>

      
      {!! Form::Close() !!}
        </div>

   </div>
  </div>
</div>

@stop
