<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Nerdsarena - Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="assets/images/ico/fab.ico">
    <link href="{{ url('elixir/backend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('elixir/backend/css/style.css') }}" rel="stylesheet">
    <link href="{{ url('elixir/backend/css/responsive.css') }}" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="{{ url('elixir/frontend/img/favicon.png') }}"/>
</head>

<body class="">
<nav class="navigation">
<div class="container-fluid">

<div class="header-logo">
    <a href="{{ url('dashboard') }}">
        <h1>DASHBOARD</h1>
    </a>
</div>

<div class="top-navigation">

<div class="menu-control hidden-xs">
    <a href="javascript:void(0)">
        <i class="fa fa-bars"></i>
    </a>
</div>


<ul>
    <li class="dropdown">
        <a class="right-sidebar" href="{{ url('dashboard/services') }}">
            <i class="fa fa-question-circle"></i>
          @if(App\Service::where('status',0)->count() > 0)  
            <span class="badge badge-red">
                {{ App\Service::where('status',0)->count() }}
            </span>
          @endif
        </a>
    </li>
    <li class="dropdown">
        <a class="right-sidebar" href="{{ url('dashboard/messages') }}">
            <i class="fa fa-envelope"></i>
          @if(App\Contact::where('status',0)->count() > 0)    
            <span class="badge badge-red">
                {{ App\Contact::where('status',0)->count() }}
            </span>
          @endif
        </a>
    </li>
    <li>
        <a href="{{ url('/') }}">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li>
        <a href="{{ url('lock') }}">
            <i class="fa fa-lock"></i>
        </a>
    </li>
    <li>
        <a href="{{ url('logout') }}">
            <i class="fa fa-power-off"></i>
        </a>
    </li>
</ul>

</div>
</div>
</nav>

<section id="main-container">


@include('backend.navigation')





<section id="min-wrapper">
 <div id="main-content">
   <div class="container-fluid">

       @yield('content')

    </div>
 </div>
</section>



<div id="change-color">
    <div id="change-color-control">
        <a href="javascript:void(0)"><i class="fa fa-magic"></i></a>
    </div>
    <div class="change-color-box">
        <ul>
            <li class="default active"></li>
            <li class="red-color"></li>
            <li class="blue-color"></li>
            <li class="light-green-color"></li>
            <li class="black-color"></li>
            <li class="deep-blue-color"></li>
        </ul>
    </div>
</div>
</section>




<script type="text/javascript" src="{{ url('elixir/backend/js/color.js') }}"></script>
<script type="text/javascript" src="{{ url('elixir/backend/js/lib/jquery-1.11.min.js') }}"></script>
<script type="text/javascript" src="{{ url('elixir/backend/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ url('elixir/backend/js/multipleAccordion.js') }}"></script>
<script src="{{ url('elixir/backend/js/switchery.min.js') }}"></script>
<script src="{{ url('elixir/backend/js/bootstrap-switch.js') }}"></script>
<script src="{{ url('elixir/backend/js/jquery.easypiechart.min.js') }}"></script>
<script type="text/javascript" src="{{ url('elixir/backend/js/chart/flot/jquery.flot.js') }}"></script>
<script type="text/javascript" src="{{ url('elixir/backend/js/chart/flot/jquery.flot.pie.js') }}"></script>
<script type="text/javascript" src="{{ url('elixir/backend/js/chart/flot/jquery.flot.resize.js') }}"></script>
<script type="text/javascript" src="{{ url('elixir/backend/js/pages/layout.js') }}"></script>
<script src="{{ url('elixir/backend/js/countUp.min.js') }}"></script>
<script src="{{ url('elixir/backend/js/skycons.js') }}"></script>
<script src="{{ url('elixir/backend/js/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ url('elixir/backend/js/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>


@yield('jsCode')

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<script src="{{ url('elixir/backend/js/lib/jquery.easing.js') }}"></script>
<script src="{{ url('elixir/backend/js/jquery.nanoscroller.min.js') }}"></script>
<script src="{{ url('elixir/backend/js/bootstrap-progressbar.min.js') }}"></script>
<script src="{{ url('elixir/backend/js/tsort.js') }}"></script>
<script src="{{ url('elixir/backend/js/jquery.tablednd.js') }}"></script>
<script src="{{ url('elixir/backend/js/jquery.dragtable.js') }}"></script>
<script src="{{ url('elixir/backend/js/editable-table/jquery.dataTables.js') }}"></script>
<script src="{{ url('elixir/backend/js/editable-table/jquery.validate.js') }}"></script>
<script src="{{ url('elixir/backend/js/editable-table/jquery.jeditable.js') }}"></script>
<script src="{{ url('elixir/backend/js/editable-table/jquery.dataTables.editable.js') }}"></script>
<script src="{{ url('elixir/backend/js/pages/table.js') }}"></script>
</body>
</html>