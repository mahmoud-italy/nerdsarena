@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Clients</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Update Client</h3>
      </div>
   
        <div class="panel-body">

@if($errors->all())                        
 <ul class="alert alert-danger padding-left-30"> 
   @foreach($errors->all() as $error)
     <li>{{ $error }}</li>
   @endforeach
 </ul>
@endif

      {!! Form::Open() !!}
           

           <div class="col-md-6">
             
             <div class="form-group">
               <label class="ar-lbl">( عربي ) Name</label>
               <input type="text" name="name" value="{{ $row->name }}" class="form-control ar-float" required>
             </div>

             <div class="form-group">
               <label>Name ( English )</label>
               <input type="text" name="name_en" value="{{ $row->name_en }}" class="form-control" required>
             </div>

             <div class="form-group">
               <label>Link</label>
               <input type="text" name="link" class="form-control" value="{{ $row->link }}">
             </div>

              <div class="form-group">
               <label>Sort</label>
               <input type="number" name="sort" value="{{ $row->sort }}" class="form-control">
             </div>

             <div class="form-group">
             <br/>
              <button class="btn btn-primary">Update</button>
              <a onclick="history.go(-1);" class="btn btn-danger"> Back</a>
             </div>

           </div>
           <div class="col-md-4">
            <div class="form-group">
               <label>Logo</label>
               <img id="logo" src="{{ url($row->logo) }}" class="img-view-th">
               <input type="file" data-image-target="#logo" name="logo" accept="image/jpeg,jpg,png">
             </div>

           </div>

      
      {!! Form::Close() !!}
        </div>

   </div>
  </div>
</div>

@stop

@section('jsCode')

<script>
function readURL(input) {
    if (input.files && input.files[0]) {
        var image = jQuery(input).data('image-target');

        var reader = new FileReader();

        reader.onload = function (e) {
            jQuery(image).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
jQuery("input[type=file]").change(function() {
   readURL(this);
});
</script>

@stop