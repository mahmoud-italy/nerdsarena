@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Videos</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Add Work</h3>
      </div>
   
        <div class="panel-body">

@if($errors->all())                        
 <ul class="alert alert-danger padding-left-30"> 
   @foreach($errors->all() as $error)
     <li>{{ $error }}</li>
   @endforeach
 </ul>
@endif

      {!! Form::Open() !!}
           

           <div class="col-md-6">

             <div class="form-group">
               <label>Title</label>
               <input type="text" name="title_en" class="form-control" required>
             </div>

             <div class="form-group">
               <label>Content</label>
               <textarea rows="7" name="content_en" class="form-control"></textarea>
             </div>
             
             <div class="form-group">
             <label>Video</label>
             <iframe id="showlink" src="" class="t-frame">
             </iframe>  
             </div>

             <div class="form-group">
               <label>Link</label>
               <input type="text" name="get_link" class="form-control" id="vlink">
               <input type="hidden" name="link" id="get-short">
             </div>

             <div class="form-group">
             <br/>
              <button class="btn btn-primary">Submit</button>
              <a onclick="history.go(-1);" class="btn btn-danger"> Back</a>
             </div>

           </div>
           <div class="col-md-6">

             <div class="form-group">
               <label class="ar-lbl">( عربي ) Title</label>
               <input type="text" name="title" class="form-control ar-float" required>
             </div>

             <div class="form-group">
               <label class="ar-lbl"> ( عربي ) Content</label>
               <textarea rows="7" name="content" class="form-control ar-float"></textarea>
             </div>

             <div class="form-group">
               <label>Sort</label>
               <input type="number" name="sort" class="form-control">
             </div>


           </div>

      
      {!! Form::Close() !!}
        </div>

   </div>
  </div>
</div>

@stop

@section('jsCode')

<script>
  $('#vlink').on('change',function(){
    var vnum = $('#vlink').val();
    var res = vnum.split("=");
    var site = "http://www.youtube.com/embed/"+res[1];
    document.getElementById('showlink').src = site;
    $('#get-short').val(res[1]);
  });
</script>

@stop