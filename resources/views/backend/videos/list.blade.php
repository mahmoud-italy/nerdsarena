@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Videos</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">All Videos ( {{ count($data) }} )</h3>
      </div>
   
        <div class="panel-body">
        
        <a href="{{ url('dashboard/videos/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add Video</a><br/><br/>

@if(Session::has('success'))
 <p class="alert alert-success">{{ Session::get('success') }}</p>
@elseif(Session::has('error'))
 <p class="alert alert-danger">{{ Session::get('error') }}</p>
@endif
          <div class="ls-editable-table table-responsive ls-table">
            <table class="table table-bordered table-striped table-bottomless" id="ls-editable-table">
             <thead>
               <tr>
                   <th class="f-row">#</th>
                   <th class="img-row">Video</th>
                   <th>Title <span class="ar">( عربي )</span></th>
                   <th class="t-date">Date</th>
                   <th class="t-sort">Sort</th>
                   <th class="f-action">Action</th>
               </tr>
             </thead>
             <tbody>
          @foreach($data as $key => $row)   
             <tr>
                 <td class="f-row">
                   @if($key == 0)
                    {{ '1' }}
                   @else
                    {{ $key+1 }}
                   @endif
                 </td>
                 <td class="img-row">
                   <img src="http://img.youtube.com/vi/{{ $row->link }}/0.jpg" class="p-frame">
                 </td>
                 <td class="ar">{{ $row->title }}</td>
                 <td class="t-date">{{ explode(' ',$row->created_at)[0] }}</td>
                 <td class="t-sort">{{ $row->sort }}</td>
                 <td class="f-action">
              {!! Form::Open(['url'=>'dashboard/videos/del/'.$row->id]) !!}   
                 	<a href="{{ url('dashboard/videos/edit/'.$row->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                  <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
              {!! Form::Close() !!}
                 </td>
             </tr>
          @endforeach
             </tbody>
            </table>
          </div>
        </div>

   </div>
  </div>
</div>

@stop