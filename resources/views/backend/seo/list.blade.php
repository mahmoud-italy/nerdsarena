@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">SEO</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Update SEO</h3>
      </div>
 


        <div class="panel-body">

@if($errors->all())                        
 <ul class="alert alert-danger padding-left-30"> 
   @foreach($errors->all() as $error)
     <li>{{ $error }}</li>
   @endforeach
 </ul>
@endif

@if(Session::has('success'))
 <p class="alert alert-success">{{ Session::get('success') }}</p>
@elseif(Session::has('error'))
 <p class="alert alert-danger">{{ Session::get('error') }}</p>
@endif

    {!! Form::Open(['files'=>true]) !!}

         <div class="col-md-6">
           <div class="form-group">
             <label> Meta Title </label>
             <textarea class="form-control" rows="5" name="meta_title_en" required>{{{ $row->meta_title_en }}}</textarea>
           </div>

           <div class="form-group">
             <label>Meta Desc</label>
             <textarea class="form-control" rows="5" name="meta_desc_en" required>{{{ $row->meta_desc_en }}}</textarea>
           </div>

           <div class="form-group">
             <label>Meta Keywords</label>
             <textarea class="form-control" rows="5" name="meta_key_en" required>{{{ $row->meta_key_en }}}</textarea>
           </div>

           <div class="form-group">
             <label>Meta Author</label>
             <textarea class="form-control" rows="5" name="meta_author_en" required>{{{ $row->meta_author_en }}}</textarea>
           </div>

           <div class="form-group">
             <br/><br/><br/>
             <button class="btn btn-primary">Submit</button>
             <a onclick="history.go(-1);" class="btn btn-danger">Back</a>
           </div>

         </div>

         <div class="col-md-6">
           <div class="form-group">
             <label class="ar-lbl"> ( عربي ) Meta Title </label>
             <textarea class="form-control ar-float" rows="5" name="meta_title" required>{{{ $row->meta_title }}}</textarea>
           </div>

           <div class="form-group">
             <label class="ar-lbl"> ( عربي ) Meta Desc</label>
             <textarea class="form-control ar-float" rows="5" name="meta_desc" required>{{{ $row->meta_desc }}}</textarea>
           </div>

           <div class="form-group">
             <label class="ar-lbl"> ( عربي ) Meta Keywords</label>
             <textarea class="form-control ar-float" rows="5" name="meta_key" required>{{{ $row->meta_key }}}</textarea>
           </div>

           <div class="form-group">
             <label class="ar-lbl"> ( عربي ) Meta Author</label>
             <textarea class="form-control ar-float" rows="5" name="meta_author" required>{{{ $row->meta_author }}}</textarea>
           </div>

           <div class="form-group">
             <label>robots.txt</label>
             <input type="file" name="robots">
           </div>

         </div>


    {!! Form::Close() !!}

        </div>

   </div>
  </div>
</div>

@stop
