<section id="left-navigation">

<div class="user-image">
 @if(Auth::check())
   @if(!empty(Auth::user()->image))
     <img src="{{ url(Auth::user()->image) }}" class="pro_img" />
   @else
     <img src="{{ url('uploads/users/whois.png') }}" class="pro_img" />
   @endif
 @else
     <img src="{{ url('uploads/users/whois.png') }}" class="pro_img" />
 @endif
    <center class="th-name"><a href="{{ url('dashboard/profile') }}" class="admin">Admin</a></center>
    <div class="user-online-status"><span class="user-status is-online"></span></div>
</div>


<div class="phone-nav-box visible-xs">
    <a class="phone-logo" href="{{ url('dashboard') }}">
        <h1>Dashboard</h1>
    </a>
    <a class="phone-nav-control" href="javascript:void(0)">
        <span class="fa fa-bars"></span>
    </a>
    <div class="clearfix"></div>
</div>


<ul class="mainNav">

<li class="active">
    <a href="{{ url('dashboard') }}">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
</li>

<li>
    <a href="{{ url('dashboard/setting') }}">
        <i class="fa fa-plug"></i> <span>Setting</span> 
    </a>
</li>

<li>
    <a href="{{ url('dashboard/sliders') }}">
        <i class="fa fa-flag"></i> <span>Sliders</span> 
    </a>
</li>

<li>
    <a href="{{ url('dashboard/customize') }}">
        <i class="fa fa-rocket"></i> <span>Customize</span> 
    </a>
</li>

<li>
    <a href="{{ url('dashboard/works') }}">
        <i class="fa fa-trophy"></i> <span>Works</span> 
    </a>
</li>

<li>
    <a href="{{ url('dashboard/videos') }}">
        <i class="fa fa-video-camera"></i> <span>Videos</span> 
    </a>
</li>

<li>
    <a href="{{ url('dashboard/clients') }}">
        <i class="fa fa-mortar-board"></i> <span>Clients</span> 
    </a>
</li>

<li>
    <a href="{{ url('dashboard/pages') }}">
        <i class="fa fa-tasks"></i> <span>Pages</span> 
    </a>
</li>


<li>
    <a href="{{ url('dashboard/services') }}">
        <i class="fa fa-gift"></i> <span>Services</span> 
    </a>
</li>

<li>
    <a href="{{ url('dashboard/seo') }}">
        <i class="fa fa-drupal"></i> <span>SEO</span> 
    </a>
</li>

<li>
    <a href="{{ url('dashboard/users') }}">
        <i class="fa fa-user-secret"></i> <span>Users</span> 
    </a>
</li>

<li style="display:none;">
    <a href="{{ url('#') }}">
        <i class="fa fa-binoculars"></i> <span>Authentication</span> 
    </a>
</li>

<li>
    <a href="{{ url('dashboard/messages') }}">
        <i class="fa fa-envelope"></i> <span>Messages</span> 
    </a>
</li>

</ul>

</section>
