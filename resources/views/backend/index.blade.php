@extends('backend.layout')
@section('content')

<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Users</h3>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
            <li class="active">Users</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-9">
        <div class="memberBox">
            <div class="memberBox-header">
                <h5>Overview Today</h5>
            </div>
            <div id="realTimeChart" class="flotChartRealTime widgetRealTime">

            </div>
            <div class="memberBox-details">
                <ul>
                    <li>
                        <div class="memberBox-title">
                            <i class="fa fa-gift"></i>
                            <h4>Services</h4>
                        </div>
                        <div class="memberBox-value up"><i class="fa fa-cubes"></i> <span>
                        {{ App\Service::whereRaw('DATE(created_at) = ?',[date('Y-m-d')])->count() }}
                        </span></div>
                    </li>
                    <li>
                        <div class="memberBox-title">
                            <i class="fa fa-eye"></i>
                            <h4>Visitor</h4>
                        </div>
                        <div class="memberBox-value down"><i class="fa fa-flag"></i> <span>
                        {{ App\Visitor::where('date',date('Y-m-d'))->count() }}
                        </span></div>
                    </li>
                    <li>
                        <div class="memberBox-title">
                            <i class="fa fa-envelope"></i>
                            <h4>Messages</h4>
                        </div>
                        <div class="memberBox-value up"><i class="fa fa-comments"></i> <span>
                        {{ App\Contact::whereRaw('DATE(created_at) = ?',[date('Y-m-d')])->count() }}
                        </span></div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="current-status-widget">
            <ul>
                <li>
                 <a href="{{ url('dashboard/clients') }}">
                    <div class="status-box">
                        <div class="status-box-icon label-light-green white">
                            <i class="fa fa-mortar-board"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="sale-view" class="dis-none"></h5>
                        <h5>{{ App\Client::count() }}</h5>
                        <p class="lightGreen"> Clients</p>
                    </div>
                    <div class="clearfix"></div>
                  </a>
                </li>
                <li>
                 <a href="{{ url('dashboard/works') }}">
                    <div class="status-box">
                        <div class="status-box-icon label-red white" id="sale-view">
                            <i class="fa fa-trophy"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="download-show" class="dis-none"></h5>
                        <h5>{{ App\Work::count() }}</h5>
                        <p class="light-blue"> Our Work</p>
                    </div>
                    <div class="clearfix"></div>
                  </a>
                </li>
                <li>
                  <a href="{{ url('dashboard/pages') }}">
                    <div class="status-box">
                        <div class="status-box-icon label-lightBlue white">
                            <i class="fa fa-tags"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="deliver-show" class="dis-none"></h5>
                        <h5>{{ App\Page::count() }}</h5>
                        <p class="light-blue"> Pages</p>
                    </div>
                    <div class="clearfix"></div>
                  </a>
                </li>
                <li>
                  <a href="{{ url('dashboard/users') }}">
                    <div class="status-box">
                        <div class="status-box-icon label-light-green white">
                            <i class="fa fa-user-secret"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="user-show" class="dis-none"></h5>
                        <h5>{{ App\User::count() }}</h5>
                        <p class="lightGreen"> Users</p>
                    </div>
                    <div class="clearfix"></div>
                  </a>
                </li>
                <li>
                 <a href="{{ url('dashboard/services') }}">
                    <div class="status-box">
                        <div class="status-box-icon label-success white">
                            <i class="fa fa-github"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="product-up" class="dis-none"></h5>
                        <h5>{{ App\Service::count() }}</h5>
                        <p class="text-success"> Services</p>
                    </div>
                    <div class="clearfix"></div>
                 </a>
                </li>
                <li>
                <a href="{{ url('dashboard/messages') }}">
                    <div class="status-box">
                        <div class="status-box-icon label-light-green white">
                            <i class="fa fa-comments"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="income-show" class="dis-none"></h5>
                        <h5>{{ App\Contact::count() }}</h5>
                        <p class="lightGreen"> Messages</p>
                    </div>
                    <div class="clearfix"></div>
                 </a>
                </li>
            </ul>
        </div>

    </div>
</div>

<div class="row home-row-top">
    <div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">
        <div class="pie-widget">
            <div id="pie-widget-1" class="chart-pie-widget" data-percent="73">
                <span class="pie-widget-count-1 pie-widget-count"></span>
            </div>
            <p>
                New Projects
            </p>
            <h5><i class="fa fa-bomb"></i> {{ App\Service::where('status',2)->count() }} </h5>

        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">
        <div class="pie-widget">
            <div id="pie-widget-2" class="chart-pie-widget" data-percent="93">
                <span class="pie-widget-count-2 pie-widget-count"></span>
            </div>
            <p>
                New Visitors
            </p>
            <h5><i class="fa fa-child"></i> {{ App\Visitor::distinct('ip_address')->count() }} </h5>

        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">
        <div class="pie-widget">
            <div id="pie-widget-3" class="chart-pie-widget" data-percent="23">
                <span class="pie-widget-count-3 pie-widget-count"></span>
            </div>
            <p>
                Total income
            </p>
            <h5><i class="fa fa-dollar"></i> 0 </h5>

        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">
        <div class="pie-widget">
            <div id="pie-widget-4" class="chart-pie-widget" data-percent="33">
                <span class="pie-widget-count-4 pie-widget-count"></span>
            </div>
            <p>
                Sale reports
            </p>
            <h5><i class="fa fa-flag"></i> 0</h5>

        </div>
    </div>
</div>


    <div class="tab-pane fade in active" style="display:none;" id="yearly">
      <div id="seriesToggleWidget" class="seriesToggleWidget"></div>
        <ul id="choicesWidget"></ul>
    </div>
    <div class="tab-pane fade" style="display:none;" id="product">
      <div id="flotPieChart" class="flotPieChartWidget"></div>
    </div>


@stop


@section('jsCode')
<script src="{{ url('elixir/backend/js/pages/dashboard.js') }}"></script>
@stop