@extends('backend.layout')
@section('content')


<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">Setting</h3>
    </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Configure Setting</h3>
      </div>
 


        <div class="panel-body">

@if($errors->all())                        
 <ul class="alert alert-danger padding-left-30"> 
   @foreach($errors->all() as $error)
     <li>{{ $error }}</li>
   @endforeach
 </ul>
@endif

@if(Session::has('success'))
 <p class="alert alert-success">{{ Session::get('success') }}</p>
@elseif(Session::has('error'))
 <p class="alert alert-danger">{{ Session::get('error') }}</p>
@endif

    {!! Form::Open(['files'=>true]) !!}

         <div class="col-md-6">
           <div class="form-group">
             <label class="ar-lbl"> ( عربي ) Title </label>
             <input type="text" class="form-control ar-float" value="{{ $title->value }}" name="title" required>
           </div>

           <div class="form-group">
             <label>Title</label>
             <input type="text" class="form-control" value="{{ $title_en->value }}" name="title_en" required>
           </div>

           <div class="form-group">
             <label>Email</label>
             <input type="email" class="form-control" value="{{ $email->value }}" name="email" required>
           </div>

           <div class="form-group">
             <label>Phone</label>
             <input type="number" class="form-control" value="{{ $phone->value }}" name="phone" required>
           </div>

           <div class="form-group">
             <button class="btn btn-primary">Submit</button>
             <a onclick="history.go(-1);" class="btn btn-danger">Back</a>
           </div>

         </div>
         <div class="col-md-4">
           <div class="form-group">
             <label>Logo</label>
             <img id="logo" src="{{ url('elixir/frontend/images/logo.png') }}" class="t-logo-img">
             <input type="file" data-image-target="#logo" name="logo" accept="image/jpeg,jpg,png">
           </div>
         </div>

    {!! Form::Close() !!}

        </div>

   </div>
  </div>
</div>

@stop

@section('jsCode')

<script>
function readURL(input) {
    if (input.files && input.files[0]) {
        var image = jQuery(input).data('image-target');

        var reader = new FileReader();

        reader.onload = function (e) {
            jQuery(image).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
jQuery("input[type=file]").change(function() {
   readURL(this);
});
</script>

@stop