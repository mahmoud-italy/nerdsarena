<?php

namespace App\Http\Controllers\backend;

use App\Work;

use Illuminate\Http\Request;
use Session;
use Input;
use File;
use Validator;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WorksController extends Controller
{
    
    public function index()
    {
      $data = Work::all();
      return view('backend.works.list',compact('data'));
    }

    public function create()
    {
        return view('backend.works.create');
    }

   
    public function store(Request $request)
    {
       $rules =
         [
          'image' => 'required|image|mimes:jpeg,jpg,png',
          'title' => 'required',
          'title_en' => 'required',
          'content' => 'required',
          'content_en' => 'required',
          'link' => 'required',
          'bs' => 'required',
          'sort' => 'numeric',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

       $work = new Work;
       $work->title = strip_tags($request->input('title'));
       $work->title_en = strip_tags($request->input('title_en'));
       $work->content = strip_tags($request->input('content'));
       $work->content_en = strip_tags($request->input('content_en')); 
       $work->link = strip_tags($request->input('link'));
       $work->bs = strip_tags($request->input('bs'));
       $work->sort = strip_tags($request->input('sort'));

       $file = strip_tags($request->file('image'));
       $path = 'uploads/works/';

       $filename = date('Y-m-d-h-i-s').$file->getClientOriginalName();
       $ext = '.'.$file->getClientOriginalExtension();
       $file->move($path,$filename.$ext);
       $work->image = $path.$filename.$ext;

    
    try {
        $work->save();
        Session::flash('success','Work Added Successfully');
    } catch (\Exception $e) {
        Session::flash('error','Work Not Added, please try again later :)');
    }
      return Redirect::to('dashboard/works');
    }

  

    public function edit($id)
    {
       $work = Work::find($id);
       if(!$work)
       {
        return Redirect::to('dashboard/works');
       }
       return view('backend.works.edit')->withrow($work);
    }

 
    public function update(Request $request, $id)
    {
        $rules =
         [
          'image' => 'image|mimes:jpeg,jpg,png',
          'title' => 'required',
          'title_en' => 'required',
          'content' => 'required',
          'content_en' => 'required',
          'link' => 'required',
          'bs' => 'required',
          'sort' => 'numeric',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

       $work = Work::find($id);
        if(!$work)
        {
            return Redirect::to('dashboard/works');
        }
       $work->title = strip_tags($request->input('title'));
       $work->title_en = strip_tags($request->input('title_en'));
       $work->content = strip_tags($request->input('content'));
       $work->content_en = strip_tags($request->input('content_en')); 
       $work->link = strip_tags($request->input('link'));
       $work->sort = strip_tags($request->input('sort'));

       $file = strip_tags($request->file('image'));

        $path = 'uploads/works/';
     if($request->has('image'))
     {
        $filename = date('Y-m-d-h-i-s').$file->getClientOriginalName();
        $ext = '.'.$file->getClientOriginalExtension();
        $file->move($path,$filename.$ext);
        $user->image = $path.$filename.$ext;
     }


    try {
        $work->save();
        Session::flash('success','Work Updated Successfully');
    } catch (\Exception $e) {
        Session::flash('error','Work Not Updated, please try again later :)');
    }
      return Redirect::to('dashboard/works');
    }

    public function destroy($id)
    {
        $work = Work::find($id);
       if(!$work)
       {
        return Redirect::to('dashboard/works');
       }
    try {
         File::Delete($work->image);
         $work->delete();
         Session::flash('success','Work Deleted Successfully');       
       } catch (\Exception $e) {
         Session::flash('error','Work Not Deleted, please try again later');  
       }
       return Redirect::back();
    }
}
