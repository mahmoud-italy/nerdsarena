<?php

namespace App\Http\Controllers\backend;

use App\Customize;

use Illuminate\Http\Request;
use Redirect;
use Session;
use Input;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CustomizeController extends Controller
{
   
    public function index()
    {
        $data = Customize::all();
        return view('backend.customize.list',compact('data'));
    }

    public function create()
    {
        return view('backend.customize.create');
    }

   
    public function store(Request $request)
    {
        $rules =
         [
          'title' => 'required|max:150',
          'title_en' => 'required|max:150',
          'type' => 'required|numeric',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

        $cust = new Customize;
        $cust->title = strip_tags($request->input('title'));
        $cust->title_en = strip_tags($request->input('title_en'));
        $cust->content = strip_tags($request->input('content'));
        $cust->content_en = strip_tags($request->input('content_en'));
        $cust->type = strip_tags($request->input('type'));
     try {
           $cust->save();
           Session::flash('success','Customize Added Successfully'); 
        } catch (\Exception $e) {
           Session::flash('error','Customize Not Added, please try again later :)');  
        }

        return Redirect::to('dashboard/customize');
    }

   
    public function edit($id)
    {
       $cust = Customize::find($id);
       if(!$cust)
       {
        return Redirect::to('dashboard/customize');
       }
       return view('backend.customize.edit')->withrow($cust);
    }

    public function update(Request $request, $id)
    {
       $rules =
         [
          'title' => 'required|max:150',
          'title_en' => 'required|max:150',
          'type' => 'required|numeric',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

        $cust = Customize::find($id);
        if(!$cust)
        {
            return Redirect::to('dashboard/customize');
        }
        $cust->title = strip_tags($request->input('title'));
        $cust->title_en = strip_tags($request->input('title_en'));
        $cust->content = strip_tags($request->input('content'));
        $cust->content_en = strip_tags($request->input('content_en'));
        $cust->type = strip_tags($request->input('type'));
     try {
           $cust->save();
           Session::flash('success','Customize Updated Successfully'); 
        } catch (\Exception $e) {
           Session::flash('error','Customize Not Updated, please try again later :)');  
        }

        return Redirect::to('dashboard/customize');
    }

    public function destroy($id)
    {
     $cust = Customize::find($id);
       if(!$cust)
       {
        return Redirect::to('dashboard/customize');
       }

      try {
          $cust->delete();
          Session::flash('success','Customize Deleted Successfully'); 
       } catch (\Exception $e) {
          Session::flash('error','Customize Not Deleted, please try again later :)'); 
       } 
       return Redirect::back();
    }
}
