<?php

namespace App\Http\Controllers\backend;

use App\Setting;
use Session;
use Redirect;
use Validator;
use Input;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
   
    public function index()
    {
    	$title = Setting::where('key','title')->first();
    	$title_en = Setting::where('key','title_en')->first();
    	$email = Setting::where('key','email')->first();
    	$phone = Setting::where('key','phone')->first();
        return view('backend.setting.list')
        ->withtitle($title)->withtitle_en($title_en)
        ->withemail($email)->withphone($phone);
    }

    public function store(Request $request)
    {
       $rules =
         [
          'title' => 'required|max:50',
          'title_en' => 'required|max:50',
          'email' => 'required|email|max:50',
          'phone' => 'required|numeric',
          'logo'=>'image|mimes:jpeg,jpg,png',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

       $title = Setting::where('key','title')->first();
       $title->value = strip_tags($request->input('title'));
       $title->save();

       $title_en = Setting::where('key','title_en')->first(); 
       $title_en->value = strip_tags($request->input('title_en'));
       $title_en->save();

       $email = Setting::where('key','email')->first();
       $email->value = strip_tags($request->input('email'));
       $email->save();

       $phone = Setting::where('key','phone')->first();
       $phone->value = strip_tags($request->input('phone'));
       $phone->save();

       $file = strip_tags($request->file('logo'));

      if($request->has('logo'))
      {
      	$path = 'elixir/frontend/images/';
      	$name = 'logo.png';
      	$file->move($path,$name);
      }

       Session::flash('success','Configuration Updated Successfully');
       return Redirect::back();
    }

}
