<?php

namespace App\Http\Controllers\backend;

use App\Contact;
use App\Contact_reply;

use Illuminate\Http\Request;
use Session;
use Redirect;
use Validator;
use Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MessagesController extends Controller
{
   
    public function index()
    {
        $data = Contact::all();
        return view('backend.messages.list',compact('data'));
    }

    public function show($id)
    {
     $con = Contact::find($id);
       if(!$con){
        return Redirect::to('dashboard/messages');
       }
       $con->status = '1';
       $con->save();
       return view('backend.messages.reply')->withrow($con);
    }

    public function reply($id,Request $request)
    {
      $rules =
         [
          'reply' => 'required',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Response::back()->withInput()->withErrors($validate);
        }

      $con = Contact::find($id);
       if(!$con){
         return Redirect::to('dashboard/messages');
       }
       $reply = new Contact_reply;
       $reply->msg_id = $id;
       $reply->content = $request->input('reply');
      try {
         $reply->save();
         Session::flash('success','Message Sent Successfully');
       } catch (\Exception $e) {
         Session::flash('error','Message Not Sent');
       } 
       
       return Redirect::back();
    }

    public function destroy($id)
    {
       $con = Contact::find($id);
       if(!$con){
        return Redirect::to('dashboard/messages');
       }

       try {
           $con->delete();

         $reply = Contact_reply::where('msg_id',$id)->get();
        foreach($reply as $msg){  
         $msg->delete();
         }

           Session::flash('success','Message Deleted Successfully');
       } catch (\Exception $e) {
           Session::flash('error','Messages Not Deleted');
       }
       return Redirect::to('dashboard/messages');
    }


}
