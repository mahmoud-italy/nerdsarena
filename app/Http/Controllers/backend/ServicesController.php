<?php

namespace App\Http\Controllers\backend;

use App\Service;
use App\Service_color;
use App\Service_file;

use File;
use Session;
use Redirect;
use Input;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ServicesController extends Controller
{
   
    public function index()
    {
        $data = Service::all();
        return view('backend.services.list',compact('data'));
    }

  
    public function show($id)
    {
       $con = Service::find($id);
       if(!$con){
        return Redirect::to('dashboard/services');
       }
       $con->status = '1';
       $con->save();
       return view('backend.services.reply')->withrow($con);
    }

    public function destroy($id)
    {
      $con = Service::find($id);
      if(!$con){
        return Redirect::to('dashboard/services');
      }
       
      try {
      

        $colors = Service_color::where('service_id',$id)->get();
      if(!empty($colors)){
        foreach($colors as $color){  
         $color->delete();
         }
        } 
      
      if(!empty($con->file)) { 
        $file = Service_file::where('upload_file',$con->file)->first();
        $file->delete();
        File::Delete($con->file);
        }
 
        $con->delete();
        Session::flash('success','Services Deleted Successfully');
      } catch (\Exception $e) {
        Session::flash('error','Services Not Deleted');
      }

     return Redirect::back();
    }
}
