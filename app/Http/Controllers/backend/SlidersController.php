<?php

namespace App\Http\Controllers\backend;

use App\Slider;

use Session;
use Redirect;
use Validator;
use Input;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SlidersController extends Controller
{
    
    public function index()
    {
        $data = Slider::all();
        return view('backend.sliders.list',compact('data'));
    }

    
    public function create()
    {
       return view('backend.sliders.create');
    }

    public function store(Request $request)
    {
       $rules =
         [
          'title' => 'required|max:100',
          'title_en' => 'required|max:100',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

       $slider = new Slider;
       $slider->title = strip_tags($request->input('title'));
       $slider->title_en = strip_tags($request->input('title_en'));
       $slider->type = strip_tags($request->input('type'));
    try {
        $slider->save();
        Session::flash('success','Slider Added Successfully');
    } catch (\Exception $e) {
        Session::flash('error','Slider Not Added, please try again later');
    }
      return Redirect::to('dashboard/sliders');
       
    }

    public function edit($id)
    {
      $slider = Slider::find($id);
     if(!$slider)
      {
         return Redirect::to('dashboard/sliders')->withrow($slider);
      }

      return view('backend.sliders.edit')->withrow($slider);
    }

    
    public function update(Request $request, $id)
    {
      $rules =
         [
          'title' => 'required|max:100',
          'title_en' => 'required|max:100',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

       $slider = Slider::find($id);
        if(!$slider)
        {
            return Redirect::to('dashboard/sliders');
        }
       $slider->title = strip_tags($request->input('title'));
       $slider->title_en = strip_tags($request->input('title_en'));
       $slider->type = strip_tags($request->input('type'));
    try {
        $slider->save();
        Session::flash('success','Slider Updated Successfully');
    } catch (\Exception $e) {
        Session::flash('error','Slider Not Updated, please try again later');
    }
      return Redirect::to('dashboard/sliders');
    }

   
    public function destroy($id)
    {
        $slider = Slider::find($id);
     if(!$slider)
      {
         return Redirect::to('dashboard/sliders');
      }
       try {
           $slider->delete();
           Session::flash('success','Slider Deleted Successfully');
       } catch (\Exception $e) {
           Session::flash('error','Slider Not Deleted, please try again later');
       }
      return Redirect::back();
    }
}
