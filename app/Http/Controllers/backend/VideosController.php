<?php

namespace App\Http\Controllers\backend;

use App\Video;

use Illuminate\Http\Request;
use Redirect;
use Session;
use Input;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class VideosController extends Controller
{
    
    public function index()
    {
      $data = Video::all();
      return view('backend.videos.list',compact('data'));
    }

    public function create()
    {
       return view('backend.videos.create');
    }

  
    public function store(Request $request)
    {
       $rules =
         [
          'title' => 'required|max:150',
          'title_en' => 'required|max:150',
          'content' => 'required',
          'content_en' => 'required',
          'link' => 'required',
          'sort' => 'numeric',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

      $video = new Video;
      $video->title = strip_tags($request->input('title'));
      $video->title_en = strip_tags($request->input('title_en'));
      $video->content = strip_tags($request->input('content'));
      $video->content_en = strip_tags($request->input('content_en'));
      $video->link = strip_tags($request->input('link'));
      $video->sort = strip_tags($request->input('sort'));
    try {
        $video->save();
        Session::flash('success','Video Added Successfully');
    } catch (\Exception $e) {
        Session::flash('error','Video Not Added, please try again later :)');
    }
      return Redirect::to('dashboard/videos');
    }

   
    
    public function edit($id)
    {
       $video = Video::find($id);
        if(!$video)
        {
            return Redirect::to('dashbord/videos');
        }
        return view('backend.videos.edit')->withrow($video);
    }

    public function update(Request $request, $id)
    {
       $rules =
         [
          'title' => 'required|max:150',
          'title_en' => 'required|max:150',
          'content' => 'required',
          'content_en' => 'required',
          'link' => 'required',
          'sort' => 'numeric',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

      $video = Video::find($id);
        if(!$video)
        {
            return Redirect::to('dashboard/videos');
        }
      $video->title = strip_tags($request->input('title'));
      $video->title_en = strip_tags($request->input('title_en'));
      $video->content = strip_tags($request->input('content'));
      $video->content_en = strip_tags($request->input('content_en'));
      $video->link = strip_tags($request->input('link'));
      $video->sort = strip_tags($request->input('sort'));
    try {
        $video->save();
        Session::flash('success','Video Updated Successfully');
    } catch (\Exception $e) {
        Session::flash('error','Video Not Updated, please try again later :)');
    }
      return Redirect::to('dashboard/videos');
    }

    
    public function destroy($id)
    {
        $video = View::find($id);
        if(!$video)
        {
            return Redirect::to('dashbord/videos');
        }
        try {
            $video->delete();
            Session::flash('success','Video Deleted Successfully');
        } catch (\Exception $e) {
            Session::flash('success','Video Not Deleted');
        }
        return Redirect::to('dashboard/videos');
    }
}
