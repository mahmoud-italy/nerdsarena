<?php

namespace App\Http\Controllers\backend;

use App\User;
use App\Seo;

use Auth;
use Validator;
use Session;
use Hash;
use Input;
use Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    
    public function index()
    {
        return view('backend.index');
    }

    /* Profile */
    public function profile()
    {
      $user = User::where('id',Auth::user()->id)->first();
      return view('backend.users.profile')->withrow($user);
    }

    public function post_profile(Request $request)
    {
    	$rules =
         [
          'name' => 'required|max:50',
          'position' => 'required|max:50',
          'email' => 'required',
          'image'=>'image|mimes:jpeg,jpg,png',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

       $user = User::where('id',Auth::user()->id)->first();
       $user->name = strip_tags($request->input('name'));
       $user->email = strip_tags($request->input('email'));

      if(!empty($request->input('password')))  
       $user->password = Hash::make(strip_tags($request->input('password')));

       $user->position = strip_tags($request->input('position')); 
       $file = strip_tags($request->file('image'));

      if(!empty($request->file('image')))
      {
        $path = 'uploads/users/';
        $regx = str_random("10");
        $filename = date('Y-m-d-h-i-s')."-".$regx;
        $ext = '.'.$file->getClientOriginalExtension();
        $file->move($path,$filename.$ext);
        $user->image = $path.$filename.$ext;
      }

    try {
        $user->save();
        Session::flash('success','User Updated Successfully');
    } catch (\Exception $e) {
        Session::flash('error','User Not Updated, please try again later :)');
    }

       return Redirect::back();
    }

   
   /* SEO */
   public function seo()
   {
    $seo = Seo::find(1);
    return view('backend.seo.list')->withrow($seo);
   }

   public function post_seo(Request $request)
   {
      $rules =
         [
          'meta_title' => 'required',
          'meta_title_en' => 'required',
          'meta_desc' => 'required',
          'meta_desc_en' => 'required',
          'meta_key' => 'required',
          'meta_key_en' => 'required',
          'meta_author' => 'required',
          'meta_author_en' => 'required',
          'robots'=>'mimes|txt',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

        $seo = new Seo;
        $seo->meta_title = strip_tags($request->input('meta_title'));
        $seo->meta_title_en = strip_tags($request->input('meta_title_en'));
        $seo->meta_desc = strip_tags($request->input('meta_desc'));
        $seo->meta_desc_en = strip_tags($request->input('meta_desc_en'));
        $seo->meta_key = strip_tags($request->input('meta_key'));
        $seo->meta_key_en = strip_tags($request->input('meta_key_en'));
        $seo->meta_author = strip_tags($request->input('meta_author'));
        $seo->meta_author_en = strip_tags($request->input('meta_author_en'));
      
        $file = $request->file('robots');
      if(!empty($file))
      {
        $path = base_path();
        $filename = 'robots.txt';
        $file->move($path,$filename);
      }

      try {
        $seo->save();
        Session::flash('success','SEO Updated Successfully');
      } catch (\Exception $e) {
        Session::flash('error','SEO Not Updated');
      }
        
        return Redirect::back();
   }
    
}
