<?php

namespace App\Http\Controllers\backend;

use App\User;

use Auth;
use Validator;
use Session;
use Redirect;
use Input;
use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    
    public function index()
    {
        $users = User::all();
        return view('backend.users.list',compact('users'));
    }

    
    public function create()
    {
        return view('backend.users.create');
    }

   
    public function store(Request $request)
    {
       $rules =
         [
          'name' => 'required|max:50',
          'position' => 'required|max:50',
          'email' => 'required|email|unique:users,email|max:50',
          'password' => 'required|min:6|max:100',
          'image'=>'image|mimes:jpeg,jpg,png',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

       $user = new User;
       $user->name = strip_tags($request->input('name'));
       $user->email = strip_tags($request->input('email'));
       $user->password = strip_tags(Hash::make($request->input('password')));
       $user->position = strip_tags($request->input('position')); 
       $file = $request->file('image');
       $rand = Str::random(5);
      if($request->has('image'))
      {
        $path = 'uploads/users/';
        $filename = date('Y-m-d-h-i-s').'-'.$rand;
        $ext = '.'.$file->getClientOriginalExtension();
        $file->move($path,$filename.$ext);
        $user->image = $path.$filename.$ext;
      }
    try {
        $user->save();
        Session::flash('success','User Added Successfully');
    } catch (\Exception $e) {
        Session::flash('error','User Not Added, please try again later :)');
    }
      return Redirect::to('dashboard/users');

    }

    
    public function show($id)
    {
      $users = User::find($id);
      if(!$users)
       {
        return Redirect::to('dashboard/users');
       }
       return view('backend.users.show')->withrow($users);
    }

    
    public function edit($id)
    {
      $users = User::find($id);
      if(!$users)
       {
        return Redirect::to('dashboard/users');
       }

       if($id == Auth::user()->id || Auth::user()->position == 'Administrator') 
         return view('backend.users.edit')->withrow($users);
       else
         return Redirect::to('dashboard/users');
    }

    
    public function update(Request $request, $id)
    {
      $rules =
         [
          'name' => 'required|max:50',
          'position' => 'required|max:50',
          'email' => 'required',
          'image'=>'image|mimes:jpeg,jpg,png',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

       $user = User::find($id);
       if(!$user)
         {
          return Redirect::to('dashboard/users');
         }

       $user->name = strip_tags($request->input('name'));
       $user->email = strip_tags($request->input('email'));

     if(!empty($request->input('password')))  
       $user->password = strip_tags(Hash::make($request->input('password')));
    
    
       $user->position = strip_tags($request->input('position')); 
       $file = $request->file('image');

      if(!empty($request->file('image')))
      {
        $path = 'uploads/users/';
        $filename = date('Y-m-d-h-i-s').$file->getClientOriginalName();
        $ext = '.'.$file->getClientOriginalExtension();
        $file->move($path,$filename.$ext);
        $user->image = $path.$filename.$ext;
      }
    try {
        $user->save();
        Session::flash('success','User Updated Successfully');
    } catch (\Exception $e) {
        Session::flash('error','User Not Updated, please try again later :)');
    }
      return Redirect::to('dashboard/users');
    }

    
    public function destroy($id)
    {
     $users = User::find($id);
      if(!$users)
       {
        return Redirect::to('dashboard/users');
       }
    
    if(!empty($users->image)) 
      {
         $path = $users->image;
         Fil::Delete($path);
      }

    try {
        $users->delete();
        Session::flash('success','User Deleted Successfully'); 
    } catch (\Exception $e) {
        Session::flash('error','User Not Deleted, plesae try again later :)');
    }
      return Redirect::to('dashboard/users');

    }
}
