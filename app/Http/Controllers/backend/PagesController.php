<?php

namespace App\Http\Controllers\backend;

use App\Page;

use Validator;
use Session;
use Redirect;
use Input;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
   
    public function index()
    {
      $data = Page::all();
      return view('backend.pages.list',compact('data'));
    }

    public function create()
    {
      return view('backend.pages.create');
    }

    
    public function store(Request $request)
    {
       $rules =
         [
          'name' => 'required|max:150',
          'name_en' => 'required|max:150',
          'content' => 'required',
          'content_en' => 'required',
          'link' => 'required',
          'sort' => 'numeric',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

       $page = new Page;
       $page->name = strip_tags($request->input('name'));
       $page->name_en = strip_tags($request->input('name_en'));
       $page->content = strip_tags($request->input('content'));
       $page->content_en = strip_tags($request->input('content_en'));
       $page->link = strip_tags($request->input('link'));
       $page->sort = strip_tags($request->input('sort'));
    
    try {
            $page->save();
            Session::flash('success','Page Added Succussfully');  
        } catch (\Exception $e) {
            Session::flash('error','Page Not Added');
        } 
        return Redirect::to('dashboard/pages');
    }

   
    public function edit($id)
    {
       $page = Page::find($id);
       if(!$page)
       {
        return Redirect::to('dashboard/pages');
       }
       return view('backend.pages.edit')->withrow($page);
    }

    
    public function update(Request $request, $id)
    {
        $rules =
         [
          'name' => 'required|max:150',
          'name_en' => 'required|max:150',
          'content' => 'required',
          'content_en' => 'required',
          'link' => 'required',
          'sort' => 'numeric',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

       $page = Page::find($id);
        if(!$page)
        {
            return Redirect::to('dashboard/pages');
        }
       $page->name = strip_tags($request->input('name'));
       $page->name_en = strip_tags($request->input('name_en'));
       $page->content = strip_tags($request->input('content'));
       $page->content_en = strip_tags($request->input('content_en'));
       $page->link = strip_tags($request->input('link'));
       $page->sort = strip_tags($request->input('sort'));
    
    try {
            $page->save();
            Session::flash('success','Page Updated Succussfully');  
        } catch (\Exception $e) {
            Session::flash('error','Page Not Updated');
        } 
        return Redirect::to('dashboard/pages');
    }

    
    public function destroy($id)
    {
       $page = Page::find($id);
       if(!$page)
       {
        return Redirect::to('dashboard/pages');
       }
       try {
           $page->delete();
           Session::flash('success','Page Deleted Succussfully');
       } catch (\Exception $e) {
           Session::flash('error','Page Not Deleted');
       }
       return Redirect::back();
    }
}
