<?php

namespace App\Http\Controllers\backend;

use App\Client;

use Illuminate\Http\Request;
use Redirect;
use Session;
use Input;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ClientsController extends Controller
{
   
    public function index()
    {
        $data = Client::all();
        return view('backend.clients.list',compact('data'));
    }

    public function create()
    {
        return view('backend.clients.create');
    }

    public function store(Request $request)
    {
       $rules =
         [
          'name' => 'required|max:150',
          'name_en' => 'required|max:150',
          'logo' => 'required|image|mimes:jpeg,jpg,png',
          'link' => 'required',
          'sort' => 'numeric',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

        $client = new Client;
        $client->name = strip_tags($request->input('name'));
        $client->name_en = strip_tags($request->input('name_en'));
        $client->link = strip_tags($request->input('link'));
        $client->sort = strip_tags($request->input('sort'));
        
        $file = $request->file('logo');

        $path = 'uploads/clients/'; 
        $filename = date('Y-m-d-h-i-s').$file->getClientOriginalName();
        $ext = '.'.$file->getClientOriginalExtension();
        $file->move($path,$filename.$ext);
        $client->logo = $path.$filename.$ext;
       
       try {
           $client->save();
           Session::flash('success','Client Added Successfully');
       } catch (\Exception $e) {
           Session::flash('error','Client Not Added, please try again');
       }
        
        return Redirect::to('dashboard/clients');
    }

    public function edit($id)
    {
      $client = Client::find($id);
      if(!$client)
      {
        return Redirect::to('dashboard/clients');
      }
      return view('backend.clients.edit')->withrow($client);
    }

    
    public function update(Request $request, $id)
    {
      $rules =
         [
          'name' => 'required|max:150',
          'name_en' => 'required|max:150',
          'logo' => 'image|mimes:jpeg,jpg,png',
          'link' => 'required',
          'sort' => 'numeric',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

        $client = Client::find($id);
        if(!$client)
        {
            return Redirect::to('dashboard/clients');
        }
        $client->name = strip_tags($request->input('name'));
        $client->name_en = strip_tags($request->input('name_en'));
        $client->link = strip_tags($request->input('link'));
        $client->sort = strip_tags($request->input('sort'));
        
        $file = $request->file('logo');
      if($request->has('logo'))
      {
        $path = 'uploads/clients/'; 
        $filename = date('Y-m-d-h-i-s').$file->getClientOriginalName();
        $ext = '.'.$file1->getClientOriginalExtension();
        $file->move($path,$filename.$ext);
        $client->logo = $path.$filename.$ext;
      }
       
       try {
           $client->save();
           Session::flash('success','Client Updated Successfully');
       } catch (\Exception $e) {
           Session::flash('error','Client Not Updated, please try again');
       }
        
        return Redirect::to('dashboard/clients');
    }

   
    public function destroy($id)
    {
      $client = Client::find($id);
      if(!$client)
      {
        return Redirect::to('dashboard/clients');
      }
       try {
           File::Delete($client->logo);
           $client->delete();
           Session::flash('success','Client Deleted Successfully');
       } catch (\Exception $e) {
           Session::flash('error','Client Not Deleted');
       }

       return Redirect::back();
    }
}
