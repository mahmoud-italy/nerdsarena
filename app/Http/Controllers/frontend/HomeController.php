<?php
namespace App\Http\Controllers\frontend;

use App\Slider;
use App\Customize;
use App\Setting;
use App\Contact;
use App\Page;
use App\Service;
use App\Service_file;
use App\Service_color;
use App\Visitor;

use Auth;
use Session;
use DB;
use Validator;
use Input;
use Redirect;
use Response;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
   
    public function index()
    {
      try {
        $visit = new Visitor;
        $visit->ip_address = $_SERVER['REMOTE_ADDR'];;
        $visit->date = date('Y-m-d');
        $visit->save();
      } catch (\Exception $e) {;}

        $slider1 = Slider::find(1);
        $slider2 = Slider::find(2);
        $slider3 = Slider::find(3);
        $slider4 = Slider::find(4);
        $service1 = Customize::find(1);
        $service2 = Customize::find(2);
        $service3 = Customize::find(3);
        $service4 = Customize::find(4);
        $service5 = Customize::find(5);
        $service6 = Customize::find(6);

        return view('frontend.index')
        ->withslider1($slider1)->withslider2($slider2)
        ->withslider3($slider3)->withslider4($slider4)
        ->withservice1($service1)->withservice2($service2)
        ->withservice3($service3)->withservice4($service4)
        ->withservice5($service5)->withservice6($service6);
    }
    
    /* Serivces */
    public function services()
    {
    	return view('frontend.services');
    }

    public function post_services(Request $request)
    {
      return DB::transaction(function()
        {
      $rules =
         [
          'name' => 'required|max:50',
          'email' => 'required|email|max:100',
          'phone' => 'numeric',
          'services' => 'required',
          'message' => 'required',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

      $serv = new Service;
      $serv->name = strip_tags(Input::get('name'));
      $serv->email = strip_tags(Input::get('email'));
      $serv->phone = strip_tags(Input::get('phone'));
      $serv->type = strip_tags(Input::get('services'));

    if(Input::has('file_uploaded')) 
      $serv->file = strip_tags(Input::get('file_uploaded'));  

      $serv->message = strip_tags(Input::get('message'));

      try {

        $serv->save();
        Session::flash('success','');

    if(Input::get('colors'))
      {
        foreach(Input::get('colors') as $color)
        {
          $colors = new Service_color;
          $colors->service_id = $serv->id;
          $colors->colors = strip_tags($color);
          $colors->save();
         }
       }
      } catch (\Exception $e) {
        Session::flash('error','');
      }
      
      return Redirect::back();
      });
    }
    
    public function upload_file(Request $request)
    {
      $rules =
         [
          'file' => 'mimes:pdf,doc,docx,jpg,jpeg,png',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

        $serv = new Service_file();

        $file = $request->file('file');
        $path = "uploads/services/";
        $regx = str_random("10");
        $filename = date('Y-m-d-s-h-i')."-".$regx;
        $ext = '.'.$file->getClientOriginalExtension();
        $file->move($path,$filename.$ext);
        $upload_file = $path.$filename.$ext;

        $serv->upload_file = $upload_file;
        $serv->save();     
 
        return Response::json([
            'success' => true,
            'upload_file' => $upload_file
        ]);
    }



    public function works()
    {
    	return view('frontend.works');
    }
    
    public function about()
    {
    	return view('frontend.about');
    }

    

    /* Contact */
    public function contact()
    {
    	return view('frontend.contact');
    }

    public function post_contact(Request $request)
    {
     $rules =
         [
          'name' => 'required|max:50',
          'email' => 'required|email|max:100',
          'phone' => 'numeric',
          'title' => 'required|max:150',
          'message' => 'required',
         ];

       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

        $con = new Contact;
        $con->name = strip_tags($request->input('name'));
        $con->email = strip_tags($request->input('email'));
        $con->phone = strip_tags($request->input('phone'));
        $con->title = strip_tags($request->input('title'));
        $con->message = strip_tags($request->input('message'));
      
      try {
          $con->save();
          Session::flash('success','');
        } catch (\Exception $e) {
          Session::flash('error','');
        } 

        return Redirect::back();
    }


    /* Page */
    public function pages($name)
    {
      $page = Page::where('link',$name)->first();
      return view('frontend.pages')->withrow($page);
    }
   

    /* Login */
    public function login()
    {
      return view('frontend.login');
    }

    public function post_login(Request $request)
    {
       $rules =
         [
          'email' => 'required|email',
          'password' => 'required|',
         ];
      
       $validate = Validator::make(Input::all(),$rules);
      if($validate->fails())
        {
          return Redirect::back()->withInput()->withErrors($validate);
        }

        $data = [
            'email'    => strip_tags($request->input('email')),
            'password' => strip_tags($request->input('password'))
            ];

      if(Auth::attempt($data,true)) {
        return Redirect::to('dashboard');
      } 
      else {
       Session::flash('error_login','');
       return Redirect::back();
      }
    }

    /* Logout */
    public function logout()
    {
      Auth::logout();
      return Redirect::to('/');
    }

    public function lock()
    {
      Auth::logout();
      return Redirect::to('dashboard');
    }

    
}
