<?php

function lang($language){
    return ucfirst(Lang::get($language));
}

function change_language($language){
    Session::set('lang',$language);
    App::setLocale($language);
    dd(App::getLocale());
}

function model_translate($model,$field)
{
    if(App::getLocale() == 'en'){
        $field = $field."_en";
        return $model->$field;
    }
    return $model->$field;
}
