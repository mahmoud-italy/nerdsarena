<?php
require_once("functions.php");
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* lang Middleware */
Route::group(['middleware' => 'lang'], function () {


Route::get('lang/{language}', function ($lang) {
    Session::set('lang',$lang);
    App::setLocale($lang); 
    return Redirect::back();
});



/* Frontend */
Route::get('/','frontend\HomeController@index');


Route::get('works','frontend\HomeController@works');
Route::get('about','frontend\HomeController@about');

/* Contact */
Route::get('contact','frontend\HomeController@contact');
Route::post('contact','frontend\HomeController@post_contact');

/* Services */
Route::get('services','frontend\HomeController@services');
Route::post('services','frontend\HomeController@post_services');
Route::post('services/upload_file','frontend\HomeController@upload_file');

/* Pages */
Route::get('page/{name}','frontend\HomeController@pages');


});


/* Login */
Route::get('auth/login','frontend\HomeController@login');
Route::post('auth/login','frontend\HomeController@post_login');

Route::get('logout','frontend\HomeController@logout');





Route::group(['middleware' => 'auth'], function () {

/* Lock */
Route::get('lock','frontend\HomeController@lock');


/* Dashboard */

Route::group(['prefix' => 'dashboard'], function () {

Route::get('/','backend\AdminController@index');

/* Profile */
Route::get('profile','backend\AdminController@profile');
Route::post('profile','backend\AdminController@post_profile');

/* Setting */
Route::get('setting','backend\SettingController@index');
Route::post('setting','backend\SettingController@store');

/* Sliders */
Route::get('sliders','backend\SlidersController@index');
Route::get('sliders/create','backend\SlidersController@create');
Route::post('sliders/create','backend\SlidersController@store');
Route::get('sliders/edit/{id}','backend\SlidersController@edit');
Route::post('sliders/edit/{id}','backend\SlidersController@update');
Route::post('sliders/del/{id}','backend\SlidersController@destroy');

/* Customize */
Route::get('customize','backend\CustomizeController@index');
Route::get('customize/create','backend\CustomizeController@create');
Route::post('customize/create','backend\CustomizeController@store');
Route::get('customize/edit/{id}','backend\CustomizeController@edit');
Route::post('customize/edit/{id}','backend\CustomizeController@update');
Route::post('customize/del/{id}','backend\CustomizeController@destroy');

/* Works */
Route::get('works','backend\WorksController@index');
Route::get('works/create','backend\WorksController@create');
Route::post('works/create','backend\WorksController@store');
Route::get('works/edit/{id}','backend\WorksController@edit');
Route::post('works/edit/{id}','backend\WorksController@update');
Route::post('works/del/{id}','backend\WorksController@destroy');

/* Videos */
Route::get('videos','backend\VideosController@index');
Route::get('videos/create','backend\VideosController@create');
Route::post('videos/create','backend\VideosController@store');
Route::get('videos/edit/{id}','backend\VideosController@edit');
Route::post('videos/edit/{id}','backend\VideosController@update');
Route::post('videos/del/{id}','backend\VideosController@destroy');

/* Clients */
Route::get('clients','backend\ClientsController@index');
Route::get('clients/create','backend\ClientsController@create');
Route::post('clients/create','backend\ClientsController@store');
Route::get('clients/edit/{id}','backend\ClientsController@edit');
Route::post('clients/edit/{id}','backend\ClientsController@update');
Route::post('clients/del/{id}','backend\ClientsController@destroy');

/* Pages */
Route::get('pages','backend\PagesController@index');
Route::get('pages/create','backend\PagesController@create');
Route::post('pages/create','backend\PagesController@store');
Route::get('pages/edit/{id}','backend\PagesController@edit');
Route::post('pages/edit/{id}','backend\PagesController@update');
Route::post('pages/del/{id}','backend\PagesController@destroy');

/* Services */
Route::get('services','backend\ServicesController@index');
Route::get('services/reply/{id}','backend\ServicesController@show');
Route::get('services/edit/{id}','backend\ServicesController@edit');
Route::post('services/edit/{id}','backend\ServicesController@update');
Route::post('services/del/{id}','backend\ServicesController@destroy');

/* SEO */
Route::get('seo','backend\AdminController@seo');
Route::post('seo','backend\AdminController@post_seo');

/* Users */
Route::get('users','backend\UsersController@index');
Route::get('users/create','backend\UsersController@create');
Route::post('users/create','backend\UsersController@store');
Route::get('users/show/{id}','backend\UsersController@show');
Route::get('users/edit/{id}','backend\UsersController@edit');
Route::post('users/edit/{id}','backend\UsersController@update');
Route::post('users/del/{id}','backend\UsersController@destroy');

/* Authentication */

/* Messages */
Route::get('messages','backend\MessagesController@index');
Route::get('messages/reply/{id}','backend\MessagesController@show');
Route::post('messages/reply/{id}','backend\MessagesController@reply');
Route::get('messages/edit/{id}','backend\MessagesController@edit');
Route::post('messages/edit/{id}','backend\MessagesController@update');
Route::post('messages/del/{id}','backend\MessagesController@destroy');


 });

});